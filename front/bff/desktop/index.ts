import dotenv = require('dotenv');
import createApplication from "./src/app";
import { Application } from "express";
import { winstonLogger } from "../../../shared/logger/winston.logger";
import { UsersService } from "./src/user/users.service";
import { OrdersService } from "./src/order/orders.service";
import SocketServer from "./src/socket/socket.server";

dotenv.config();

const usersService: UsersService = new UsersService(
    process.env.USERS_SERVICE,
    process.env.AUTH_SERVICE
);

const ordersService: OrdersService = new OrdersService(
    process.env.TRANSACTIONS_SERVICE,
);

const app: Application = createApplication({
    usersService,
    ordersService
});
const port: any = process.env.PORT || 3000;

const socketServer: SocketServer = new SocketServer(+process.env.SOCKET_PORT);
socketServer.listen();

app.listen(port);

winstonLogger.info(`Application is listening on port: ${port}`);