import { Application, NextFunction, RequestHandler, Router, Response, Request} from "express";
import express = require("express");
import cors = require("cors");
import bodyParser = require("body-parser");
import requestId from "./middleware/request-id.middleware";
import { HttpError } from "../../../../shared/error/http.error";
import { errorHandler } from "../../../../shared/middleware/error.middleware";

interface ServerOptions {
    requestLogger: RequestHandler;
    routes: Router;
}

export default class Server {
    private app: Application;

    constructor(options: ServerOptions) {
        this.app = express();

        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());
        this.app.use(cors());
        this.app.use(requestId);

        this.app.use(options.requestLogger);
        this.app.use('/api', options.routes);
        this.app.use((req: Request, res: Response, next: NextFunction) => {
            next(new HttpError('Not found', 404));
        });
        this.app.use(errorHandler);
    }

    getApp(): Application {
        return this.app;
    }
}