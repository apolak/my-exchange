import * as request from 'superagent';
import { winstonLogger } from "../../../../../shared/logger/winston.logger";

export default interface OrdersServiceClient
{
    order(requestId: string, token: string, type: string, unitPrice: number, amount: number, pair: {first: string, second: string}): Promise<any>;
    cancel(requestId: string, token: string, id: string): Promise<any>
}

export class OrdersService implements OrdersServiceClient {
    constructor(
        private transactionServiceUrl: string
    ) {}

    async cancel(requestId: string, token: string, id: string): Promise<any> {
        return new Promise((resolve, reject) => {
            request
                .delete(`${this.transactionServiceUrl}/api/accounts/order/${id}?requestId=${requestId}`)
                .set('x-auth-token', token)
                .then(response => {
                    resolve()
                })
                .catch(err => {
                    winstonLogger.error(err);
                    reject(err);
                })
        });
    }

    async order(requestId: string, token: string, type: string, unitPrice: number, amount: number, pair: {first: string, second: string}): Promise<any> {
        return new Promise((resolve, reject) => {
            request
                .post(`${this.transactionServiceUrl}/api/accounts/order?requestId=${requestId}`)
                .set('x-auth-token', token)
                .send({
                    type,
                    unitPrice,
                    amount,
                    pair
                })
                .then(response => {
                    resolve()
                })
                .catch(err => {
                    winstonLogger.error(err);
                    reject(err);
                })
        });
    }

    async deposit(requestId: string, token: string, currency: string, amount: number): Promise<any> {
        return new Promise((resolve, reject) => {
            request
                .post(`${this.transactionServiceUrl}/api/accounts/deposit?requestId=${requestId}`)
                .set('x-auth-token', token)
                .send({
                    currency,
                    amount
                })
                .then(response => {
                    resolve()
                })
                .catch(err => {
                    winstonLogger.error(err);
                    reject(err);
                })
        });
    }

    async withdraw(requestId: string, token: string, currency: string, amount: number): Promise<any> {
        return new Promise((resolve, reject) => {
            request
                .post(`${this.transactionServiceUrl}/api/accounts/withdraw?requestId=${requestId}`)
                .set('x-auth-token', token)
                .send({
                    currency,
                    amount
                })
                .then(response => {
                    resolve()
                })
                .catch(err => {
                    winstonLogger.error(err);
                    reject(err);
                })
        });
    }
}