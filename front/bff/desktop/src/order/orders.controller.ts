import { Request, Response, NextFunction } from "express";
import { OrdersService } from "./orders.service";

export default class OrdersController
{
    constructor(private ordersService: OrdersService) {
        this.order = this.order.bind(this);
        this.cancel = this.cancel.bind(this);
    }

    cancel(req: Request, res: Response, next: NextFunction): void {
        this.ordersService.cancel(res.locals.requestId, req.header('x-auth-token'), req.params.id)
            .then(() => {
                res
                    .status(200)
                    .send();
            })
            .catch(next);
    }

    order(req: Request, res: Response, next: NextFunction): void {
        this.ordersService.order(res.locals.requestId, req.header('x-auth-token'),req.body.type, req.body.unitPrice, req.body.amount, req.body.pair)
            .then(() => {
                res
                    .status(201)
                    .send();
            })
            .catch(next)
    }
}