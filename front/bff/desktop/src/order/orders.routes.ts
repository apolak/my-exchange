import { Router } from 'express';
import * as express from 'express';
import { body, header, query } from "express-validator/check";
import OrdersController from "./orders.controller";
import { OrdersService } from "./orders.service";

export interface OrdersRoutesOptions {
    ordersService: OrdersService
}

export const create = (options: OrdersRoutesOptions) => {
    const router: Router = express.Router();
    const ordersController: OrdersController = new OrdersController(options.ordersService);

    router.post('/', [
        body('unitPrice').exists().withMessage('Invalid unit price'),
        body('amount').exists().withMessage('Invalid amount'),
        body('pair.first').exists().withMessage('Invalid pair'),
        body('pair.second').exists().withMessage('Invalid pair'),
        body('type').exists().withMessage('Invalid type'),
        header('x-auth-token').exists().withMessage('Invalid auth token')
    ], ordersController.order);

    router.delete('/:id', [
        query('id').exists().withMessage('Invalid id'),
        header('x-auth-token').exists().withMessage('Invalid auth token')
    ], ordersController.cancel);

    return router;
};