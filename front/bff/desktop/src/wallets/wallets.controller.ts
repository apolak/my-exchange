import { Request, Response, NextFunction } from "express";
import { OrdersService } from "../order/orders.service";

export default class WalletsController
{
    constructor(private ordersService: OrdersService) {
        this.deposit = this.deposit.bind(this);
        this.withdraw = this.withdraw.bind(this);
    }

    deposit(req: Request, res: Response, next: NextFunction): void {
        this.ordersService.deposit(res.locals.requestId, req.header('x-auth-token'),req.body.currency, req.body.amount)
            .then(() => {
                res
                    .status(201)
                    .send();
            })
            .catch(next);
    }

    withdraw(req: Request, res: Response, next: NextFunction): void {
        this.ordersService.withdraw(res.locals.requestId, req.header('x-auth-token'),req.body.currency, req.body.amount)
            .then(() => {
                res
                    .status(201)
                    .send();
            })
            .catch(next);
    }
}