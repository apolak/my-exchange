import { Router } from 'express';
import * as express from 'express';
import { body, header } from "express-validator/check";
import WalletsController from "./wallets.controller";
import { OrdersService } from "../order/orders.service";

export interface WalletsRoutesOptions {
    ordersService: OrdersService
}

export const create = (options: WalletsRoutesOptions) => {
    const router: Router = express.Router();
    const walletsController: WalletsController = new WalletsController(options.ordersService);

    router.post('/deposit', [
        body('currency').exists().withMessage('Invalid currency'),
        body('amount').exists().withMessage('Invalid amount'),
        header('x-auth-token').exists().withMessage('Invalid auth token')
    ], walletsController.deposit);

    router.post('/withdraw', [
        body('currency').exists().withMessage('Invalid currency'),
        body('amount').exists().withMessage('Invalid amount'),
        header('x-auth-token').exists().withMessage('Invalid auth token')
    ], walletsController.withdraw);

    return router;
};