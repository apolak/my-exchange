import { Router } from "express";
import * as express from "express";
import { create as createUsersRoutes, UsersRoutesOptions } from "./user/users.routes";
import { create as createOrdersRoutes, OrdersRoutesOptions } from "./order/orders.routes";
import { create as createWalletsRoutes, WalletsRoutesOptions } from "./wallets/wallets.routes";

interface RoutesOptions extends UsersRoutesOptions, OrdersRoutesOptions, WalletsRoutesOptions{}

export const create = (options: RoutesOptions) => {
    const router: Router = express.Router();

    router.use('/users', createUsersRoutes(options));
    router.use('/orders', createOrdersRoutes(options));
    router.use('/wallets', createWalletsRoutes(options));

    return router;
};