import { User } from './user.model'
import * as request from 'superagent';
import { winstonLogger } from "../../../../../shared/logger/winston.logger";

export default interface UsersServiceClient
{
    register(requestId: string, email: string, password: string): Promise<any>;
    login(requestId: string, email: string, password: string): Promise<any>;
}

export class UsersService implements UsersServiceClient {
    constructor(
        private usersServiceUrl: string,
        private authServiceUrl: string
    ) {}

    async register(requestId: string, email: string, password: string): Promise<any> {
        return new Promise((resolve, reject) => {
            request
                .post(`${this.usersServiceUrl}/api/users?requestId=${requestId}`)
                .send({
                    email,
                    password
                })
                .then(response => {
                    resolve(
                        new User(
                            response.body.id,
                            response.body.email
                        )
                    )
                })
                .catch(err => {
                    winstonLogger.error(err);
                    reject(err);
                })
        });
    }

    async login(requestId: string, email: string, password: string): Promise<any> {
        const tokenRequest: any = await request
            .post(`${this.authServiceUrl}/api/auth?requestId=${requestId}`)
            .send({
                email,
                password
            });

        return {
            token: tokenRequest.body.accessToken,
            expiresAt: tokenRequest.body.expiresAt,
        };
    }

    async me(requestId: string, userId: string): Promise<any> {
        return new Promise((resolve, reject) => {
            request
                .get(`${this.usersServiceUrl}/api/users/${userId}?requestId=${requestId}`)
                .then(response => {
                    resolve(response.body)
                })
                .catch(err => {
                    winstonLogger.error(err);
                    reject(err);
                })
        });
    }
}