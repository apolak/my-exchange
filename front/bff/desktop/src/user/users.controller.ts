import { Request, Response, NextFunction } from "express";
import { UsersService } from "./users.service";
import { User } from "./user.model";

export default class UsersController
{
    constructor(private usersService: UsersService) {
        this.register = this.register.bind(this);
        this.authenticate = this.authenticate.bind(this);
        this.me = this.me.bind(this);
    }

    register(req: Request, res: Response, next: NextFunction): void {
        this.usersService.register(res.locals.requestId, req.body.email, req.body.password)
            .then((user: User) => {
                res
                    .status(201)
                    .send(user.toJSON());
            })
            .catch(next);
    }

    authenticate(req: Request, res: Response, next: NextFunction): void {
        this.usersService.login(res.locals.requestId, req.body.email, req.body.password)
            .then((response: any) => {
                res
                    .status(200)
                    .send(response);
            })
            .catch(next);
    }

    me(req: Request, res: Response, next: NextFunction): void {
        this.usersService.me(res.locals.requestId, res.locals.userId)
            .then((response: any) => {
                res
                    .status(200)
                    .send(response);
            })
            .catch(next);
    }
}