import { Router } from 'express';
import * as express from 'express';
import UsersController from "./users.controller";
import { body } from "express-validator/check";
import { userIdMiddleware } from "../../../../../shared/middleware/user-id.middleware";
import { UsersService } from "./users.service";

export interface UsersRoutesOptions {
    usersService: UsersService
}

export const create = (options: UsersRoutesOptions) => {
    const router: Router = express.Router();
    const usersController: UsersController = new UsersController(options.usersService);

    router.use('/me', userIdMiddleware);

    router.post('/register', [
        body('email').isEmail().withMessage('Invalid email address').trim().normalizeEmail(),
        body('password').isLength({min: 1}).withMessage('Missing password')
    ], usersController.register);
    router.post('/auth', [
        body('email').isEmail().withMessage('Invalid email address').trim().normalizeEmail(),
        body('password').isLength({min: 1}).withMessage('Missing password')
    ], usersController.authenticate);
    router.get('/me', usersController.me);

    return router;
};