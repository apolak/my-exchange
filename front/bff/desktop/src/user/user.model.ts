export interface UserInterface {
    getId(): string;
    getEmail(): string;
}

export class User implements UserInterface {
    constructor(
        private id: string,
        private email: string,
    ) {}

    getId(): string {
        return this.id;
    }

    getEmail(): string {
        return this.email;
    }

    toJSON(): any {
        return {
            id: this.id,
            email: this.email
        }
    }
}