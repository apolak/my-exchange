import WebSocket = require("ws");
import urlParser = require('url');
import { winstonLogger } from "../../../../../shared/logger/winston.logger";
import { OPEN } from "ws";

export default class SocketServer {
    private server: WebSocket.Server;

    constructor(
        port: number
    ) {
        this.server = new WebSocket.Server({
            port
        });

        winstonLogger.info(`Socket works on port: ${port}`);
    }

    listen(): void{
        this.server.on('connection', (client: any, req) => {
            const url: any = urlParser.parse(req.url, true);
            client.accessToken = url.query.accessToken;

            const transactionsSocket = new WebSocket(`${process.env.TRANSACTIONS_SERVICE_SOCKET}?accessToken=${client.accessToken}`);
            const currenciesSocket = new WebSocket(`${process.env.CURRENCIES_SERVICE_SOCKET}?accessToken=${client.accessToken}`);

            transactionsSocket.on("message", (message: any) => {
                if (client.readyState === OPEN) {
                    client.send(message);
                }
            });

            currenciesSocket.on("message", (message: any) => {
                if (client.readyState === OPEN) {
                    client.send(message);
                }
            });

            client.on("error", (err: any) => {
                winstonLogger.info("Error caught: ");
                winstonLogger.info(err.stack)
            });

            client.on("end", (code: any, reason: any) => {
                winstonLogger.info('Connection Lost');
                transactionsSocket.close();
                currenciesSocket.close();
            });
        });
    }
}