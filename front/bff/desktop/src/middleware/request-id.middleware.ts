import { NextFunction, Request, Response } from "express";
import uuid = require('uuid/v4');

const requestId = (req: Request, res: Response, next: NextFunction) => {
    const id: string = uuid();

    res.locals.requestId = id;

    next();
};

export default requestId;