import React from 'react';

const Orders = ({ name, logged, orders, cancel, onCancel }) => {
  if (!logged) {
    return null;
  }

  return <ul className={"list-group"}>
    <li className="list-group-item active">{name}</li>
    {
      orders.map((order, index) => {
        if (cancel) {
          return <li key={index} className={"list-group-item d-flex"}>
            {order.unitPrice} * {order.amount} {order.pair.first}, {order.unitPrice * order.amount} {order.pair.second}
            <button className={"btn btn-danger"} onClick={() => onCancel(order._id)}>X</button>
          </li>
        }

        return <li key={index} className={"list-group-item"}>
          {order.unitPrice} * {order.amount} {order.pair.first}, {order.unitPrice * order.amount} {order.pair.second}
        </li>
      })
    }
  </ul>;
};

export default Orders;