import React, { Component } from 'react';

export default class OrderComponent extends Component {
  constructor() {
    super();

    this.state = {
      type: 'buy',
      unitPrice: '',
      amount: ''
    };

    this.handleAmountChange = this.handleAmountChange.bind(this);
    this.handleUnitPriceChange = this.handleUnitPriceChange.bind(this);
  }

  handleAmountChange(event) {
    this.setState({amount: event.target.value});
  }

  handleUnitPriceChange(event) {
    this.setState({unitPrice: event.target.value});
  }

  render() {
    if (!this.props.logged) {
      return null;
    }

    return <div className={"mb-2"}>
        <div className="form-group">
          <div className="form-check form-check-inline">
            <label className={"form-check-label"}>Buy:</label>
            <input className={"form-check-input ml-2"} type="radio" name="type" checked={this.state.type === 'buy'} value={'buy'} onChange={() => this.setState({type: 'buy', unitPrice: '', amount: ''})}/>
          </div>
          <div className="form-check form-check-inline">
            <label className={"form-check-label"}>Sell:</label>
            <input className={"form-check-input ml-2"} type="radio" name="type" checked={this.state.type === 'sell'} value={'sell'} onChange={() => this.setState({type: 'sell', unitPrice: '', amount: ''})}/>
          </div>
        </div>
        <div className="form-group form-inline">
          <input className={"form-control"} type="text" value={this.state.unitPrice} onChange={this.handleUnitPriceChange} placeholder={"Price"}/>
          <input className={"form-control ml-2"} type="text" value={this.state.amount} onChange={this.handleAmountChange} placeholder={"Amount"}/>
          <p className={"ml-2 mb-0"}>{(this.state.unitPrice !== '' && this.state.amount !== '' ? this.state.unitPrice * this.state.amount : 0)}</p>
        </div>
        <div className="form-group">
            <button className={"btn btn-primary"} onClick={() => {
              this.props.onOrder({
                pair: {
                  first: 'BTC',
                  second: 'USD'
                },
                amount: +this.state.amount,
                unitPrice: +this.state.unitPrice,
                type: this.state.type
              });

              this.setState({
                unitPrice: '',
                amount: ''
              })
            }}>Order</button>
      </div>
    </div>
  }
}