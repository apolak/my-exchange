import React, { Component } from 'react';

export default class DepositWithdrawComponent extends Component {
  constructor() {
    super();

    this.state = {
      type: 'deposit',
      amount: '',
      currency: 'usd'
    };

    this.handleAmountChange = this.handleAmountChange.bind(this);
    this.handleCurrencyChange = this.handleCurrencyChange.bind(this);
  }

  handleAmountChange(event) {
    this.setState({amount: event.target.value});
  }

  handleCurrencyChange(event) {
    this.setState({currency: event.target.value});
  }

  render() {
    if (!this.props.logged) {
      return null;
    }

    return <div className={"mb-2"}>
        <div className="form-group">
          <div className="form-check form-check-inline">
            <label className={"form-check-label"}>Deposit:</label>
            <input className={"form-check-input ml-2"} type="radio" name="deposit-type" checked={this.state.type === 'deposit'} value={'deposit'} onChange={() => this.setState({type: 'deposit',  amount: ''})}/>
          </div>
          <div className="form-check form-check-inline">
            <label className={"form-check-label"}>Withdraw:</label>
            <input className={"form-check-input ml-2"} type="radio" name="deposit-type" checked={this.state.type === 'withdraw'} value={'withdraw'} onChange={() => this.setState({type: 'withdraw', amount: ''})}/>
          </div>
        </div>
        <div className="form-group form-inline">
            <select className={"form-control"} value={this.state.currency} onChange={this.handleCurrencyChange}>
              {
                this.props.wallets.map((wallet, index) => <option value={wallet.currency} key={index}>{wallet.currency}</option>)
              }
            </select>
            <input className={"form-control ml-2"} type="text" value={this.state.amount} onChange={this.handleAmountChange} placeholder={"Amount"}/>
        </div>
        <div className="form-group">
            <button className={"btn btn-primary"} onClick={() => {
              if (this.state.type === 'deposit') {
                this.props.onDeposit({
                  amount: +this.state.amount,
                  currency: this.state.currency
                });
              }

              if (this.state.type === 'withdraw') {
                this.props.onWithdraw({
                  amount: +this.state.amount,
                  currency: this.state.currency
                });
              }

              this.setState({
                amount: ''
              })
            }}>{ this.state.type === 'deposit' ? 'Deposit' : 'Withdraw' }</button>
      </div>
    </div>
  }
}