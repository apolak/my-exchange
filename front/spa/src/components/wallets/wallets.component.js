import React from 'react';

const WalletsComponent = ({logged, wallets}) => {
  if (!logged) {
    return null;
  }

  return <div className={"d-flex p-2"}>
      {
        wallets.map((wallet, index) => <p key={index} className={"mr-2"}>
            {wallet.currency}: {wallet.available} ({wallet.locked})
        </p>)
      }
  </div>;
};

export default WalletsComponent;