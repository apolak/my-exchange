import React, { Component } from 'react';
import LoginComponent from "../login/login.component";
import UserComponent from "../user/user.component";
import OrderComponent from "../order/order.component";
import WalletsComponent from "../wallets/wallets.component";
import DepositWithdrawComponent from "../wallets/deposit-withdraw.component";
import Transactions from "../latest-transactions/transactions.component";
import Orders from "../order/orders.component";

export default class AppComponent extends Component {
  constructor() {
    super();

    this.state = {
      logged: false,
      accessToken: null,
      user: null,
      socket: null,
      wallets: [],
      rates: {},
      transactions: [],
      orders: {
        buy: [],
        sell: []
      }
    };

    this.onLogin = this.onLogin.bind(this);
    this.onRegister = this.onRegister.bind(this);
    this.onOrder = this.onOrder.bind(this);
    this.onDeposit = this.onDeposit.bind(this);
    this.onWithdraw = this.onWithdraw.bind(this);
    this.onOrderCancel = this.onOrderCancel.bind(this);
  }

  onDeposit(deposit) {
    fetch(`${process.env.REACT_APP_API}/wallets/deposit`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-auth-token': this.state.accessToken
      },
      method: "POST",
      body: JSON.stringify(deposit)
    });
  }

  onWithdraw(withdraw) {
    fetch(`${process.env.REACT_APP_API}/wallets/withdraw`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-auth-token': this.state.accessToken
      },
      method: "POST",
      body: JSON.stringify(withdraw)
    });
  }

  onOrderCancel(order) {
    fetch(`${process.env.REACT_APP_API}/orders/${order}`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-auth-token': this.state.accessToken
      },
      method: "DELETE"
    });
  }

  onOrder(order) {
    fetch(`${process.env.REACT_APP_API}/orders`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-auth-token': this.state.accessToken
      },
      method: "POST",
      body: JSON.stringify(order)
    });
  }

  fetchMe() {
    fetch(`${process.env.REACT_APP_API}/users/me`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-auth-token': this.state.accessToken
      }
    })
      .then((response) => response.json())
      .then(data => {
        this.setState({
          user: {
            email: data.email,
            id: data.id
          }
        });

        const socket = new WebSocket(`${process.env.REACT_APP_WEBSOCKET}?accessToken=${this.state.accessToken}`);

        socket.onmessage = (event) => {
          const message = JSON.parse(event.data);

          if(message.type==='latest-transactions') {
            this.setState({
              transactions: message.body
            });
          }

          if(message.type==='new-transactions') {
            this.setState({
              transactions: [
                ...message.body,
                ...this.state.transactions
              ]
            });
          }

          if(message.type==='latest-orders') {
            this.setState({
              orders: message.body
            });
          }

          if (message.type === 'rates-update') {
            this.setState({
              rates: {
                ...this.state.rates,
                [`${message.body.from.code}/${message.body.to.code}`]: message.body.rate
              }
            });
          }

          if (message.type === 'wallets') {
            const wallets = [];

            Object.keys(message.body).forEach(key => {
              wallets.push({
                currency: key,
                available: message.body[key].available,
                locked: message.body[key].locked
              })
            });

            this.setState({
              wallets
            });
          }
        }
      });
  }

  onLogin(email, password) {
    fetch(`${process.env.REACT_APP_API}/users/auth`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({
        email,
        password
      })
    })
      .then((response) => response.json())
      .then(data => {
        this.setState({
          logged: true,
          accessToken: data.token
        });

        this.fetchMe();
      });
  }

  onRegister(email, password) {
    fetch(`${process.env.REACT_APP_API}/users/register`, {
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      method: "POST",
      body: JSON.stringify({
        email,
        password
      })
    })
      .then((response) => response.json())
      .then(data => {
        this.onLogin(email, password);
      });
  }

  render() {
    return (
        <div className="container">
            <nav className={"navbar navbar-light bg-light mb-1"}>
              <LoginComponent logged={this.state.logged} onLogin={this.onLogin} onRegister={this.onRegister}/>
              <UserComponent user={this.state.user} rates={this.state.rates}/>
            </nav>
            <WalletsComponent logged={this.state.logged} wallets={this.state.wallets}/>
            <DepositWithdrawComponent logged={this.state.logged} wallets={this.state.wallets} onDeposit={this.onDeposit} onWithdraw={this.onWithdraw}/>
            <OrderComponent logged={this.state.logged} onOrder={this.onOrder}/>
            <div className={"row"}>
              <div className={"col-3"}>
                   <Orders logged={this.state.logged} name={"Buy"} orders={this.state.orders.buy}/>
              </div>
              <div className={"col-3"}>
                  <Orders logged={this.state.logged} name={"Sell"} orders={this.state.orders.sell}/>
              </div>
              <div className={"col-3"}>
                <Transactions logged={this.state.logged} transactions={this.state.transactions.filter((transaction) => transaction.type === 'sell')}/>
              </div>
              <div className={"col-3"}>
                <Orders logged={this.state.logged} name={"My orders"} orders={[
                  ...this.state.orders.buy.filter((order) => order.userId === this.state.user.id),
                  ...this.state.orders.sell.filter((order) => order.userId === this.state.user.id)
                ]} cancel={true} onCancel={this.onOrderCancel}/>
              </div>
            </div>
        </div>
    );
  }
}
