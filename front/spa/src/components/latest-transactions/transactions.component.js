import React from 'react';

const Transactions = ({ logged, transactions }) => {
  if (!logged) {
    return null;
  }

  return <ul className={"list-group"}>
    <li className="list-group-item active">Transactions</li>
    {
      transactions.map((transaction, index) => <li key={index} className={"list-group-item"}>
        {transaction.unitPrice} * {transaction.amount} {transaction.pair.first}, {transaction.unitPrice * transaction.amount} {transaction.pair.second}
      </li>)
    }
  </ul>;
};

export default Transactions;