import React from 'react';

const UserComponent = ({user, rates}) => {
  if (!user) {
    return null;
  }

  return <div className="form-inline w-100">
    <p className={"mb-0"}>{user.email}</p>
    {
      Object.keys(rates).map((key) => <p className={"ml-auto mb-0"} key={key}>{key}: {rates[key]}</p>)
    }
  </div>;
};

export default UserComponent;