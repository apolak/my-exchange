import React, { Component } from 'react';

export default class LoginComponent extends Component {
  constructor() {
    super();

    this.state = {
      email: '',
      password: ''
    };

    this.handleEmailChange = this.handleEmailChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
  }

  handlePasswordChange(event) {
    this.setState({password: event.target.value});
  }

  handleEmailChange(event) {
    this.setState({email: event.target.value});
  }

  render() {
    if (this.props.logged) {
      return null;
    }

    return <div className="form-inline w-100">
        <input className={"form-control mr-sm-2"} type="text" placeholder="email" value={this.state.email} onChange={this.handleEmailChange}/>
        <input className={"form-control mr-sm-2"} type="password" placeholder="password" value={this.state.password} onChange={this.handlePasswordChange}/>
        <button className={"btn btn-primary ml-auto"} onClick={() => this.props.onLogin(this.state.email, this.state.password)}>Login</button>
        <button className={"btn btn-primary ml-2"} onClick={() => this.props.onRegister(this.state.email, this.state.password)}>Register</button>
    </div>
  }
}