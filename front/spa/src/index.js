import React from 'react';
import ReactDOM from 'react-dom';
import AppComponent from './components/app/app.component';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<AppComponent />, document.getElementById('root'));
registerServiceWorker();
