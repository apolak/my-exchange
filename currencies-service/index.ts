import mongodb = require('mongodb');
import dotenv = require('dotenv');
import { Db, MongoClient } from "mongodb";
import RatesRepository, { MongoDBRatesRepository } from "./src/rates/rates.repository";
import JobsSchedule from "./src/jobs.schedule";
import FetchRates from "./src/jobs/fetch-rates.job";
import { Server } from "ws";
import { winstonLogger } from "../shared/logger/winston.logger";

dotenv.config();

const mongoClient: MongoClient = mongodb.MongoClient;

mongoClient
    .connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/currencies`)
    .then((db: Db) => {
        winstonLogger.info('Database connected');

        const ratesRepository: RatesRepository = new MongoDBRatesRepository(db, 'rates');

        const socketServer: Server = new Server({ port: +process.env.WS_PORT});
        socketServer.on('connection', (client) => {
            client.on("error", (err) => {
                winstonLogger.info("Error caught: ");
                winstonLogger.info(err.stack)
            });

            client.on("end", (code, reason) => {
                winstonLogger.info('Connection Lost')
            });

            ratesRepository.findByCode('BTC', 1).then((rates) => {
               if (rates.length > 0) {
                   client.send(JSON.stringify({
                       type: "rates-update",
                       body: rates[0].toJSON()
                   }))
               }
            });
        });

        const jobsSchedule: JobsSchedule = new JobsSchedule(
            [
                new FetchRates('btcusd', 'BTC', 'USD', ratesRepository, socketServer)
            ]
        );

        jobsSchedule.run();

        winstonLogger.info(`Application is listening on socket: ${process.env.WS_PORT}`);
    })
    .catch((err) => winstonLogger.error(err));