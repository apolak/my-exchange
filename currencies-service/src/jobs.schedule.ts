import Job from "./jobs/job";
import schedule = require('node-schedule');

export default class JobsSchedule {
    private activeJobs: any[] = [];

    constructor(
        private jobs: Job[]
    ) {}

    run() {
        this.jobs.forEach((job: Job) => {
            this.activeJobs.push(
                schedule.scheduleJob(
                    job.getTiming(),
                    job.invoke.bind(null, job.getArguments())
                )
            )
        });
    }
}