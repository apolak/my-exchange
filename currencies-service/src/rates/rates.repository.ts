import { Db, WriteOpResult } from "mongodb";
import { Currency, Rate } from "./rate.model";

export default interface RatesRepository
{
    findByCode(code: string, limit: number): Promise<Rate[]>;
    save(rate: Rate): Promise<boolean>;
}

export class MongoDBRatesRepository implements RatesRepository {
    constructor(
        private connection: Db,
        private collection: string
    ) {}

    async findByCode(code: string, limit: number): Promise<Rate[]> {
        return await this.connection.collection(this.collection)
            .find({
                "from.code": code.toLowerCase()
            })
            .sort({
                date: -1
            })
            .limit(limit)
            .toArray()
            .then((ratesData: any[]) => {
                if (ratesData.length === 0) {
                    return [];
                }

                return ratesData.map((rate) => new Rate(
                    new Currency(rate.from.code, rate.from.code),
                    new Currency(rate.to.code, rate.to.code),
                    rate.rate,
                    rate.date
                ))
            });
    }

    async save(rate: Rate): Promise<boolean> {
        return await this.connection.collection(this.collection).insert({
            rate: rate.getRate(),
            to: {
                code: rate.getTo().getCode()
            },
            from: {
                code: rate.getFrom().getCode()
            },
            date: rate.getDate()
        }).then((result: WriteOpResult) => {
            return true;
        })
    }
}