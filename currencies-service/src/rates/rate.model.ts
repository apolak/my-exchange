export class Currency {
    constructor(
        private name: string,
        private code: string
    ) {}

    getCode(): string {
        return this.code;
    }

    getName(): string {
        return this.name;
    }
}

export class Rate {
    constructor(
        private from: Currency,
        private to: Currency,
        private rate: number,
        private date: number
    ) {}

    getFrom(): Currency {
        return this.from;
    }

    getTo(): Currency {
        return this.to;
    }

    getRate(): number {
        return this.rate;
    }

    getDate(): number {
        return this.date;
    }

    toJSON(): any {
        return {
            from: {
                code: this.from.getCode(),
                name: this.from.getName()
            },
            to: {
                code: this.to.getCode(),
                name: this.to.getName()
            },
            rate: this.rate,
            date: this.date
        }
    }
}