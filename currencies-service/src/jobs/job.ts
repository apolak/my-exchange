export default interface Job {
    getTiming(): string;
    getArguments(): any;
    invoke(args: any): void;
}