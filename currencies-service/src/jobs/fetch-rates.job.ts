import Job from "./job";
import RatesRepository from "../rates/rates.repository";
import * as request from 'superagent';
import { Currency, Rate } from "../rates/rate.model";
import { Server, OPEN } from "ws";
import { winstonLogger } from "../../../shared/logger/winston.logger";

export default class FetchRates implements Job
{
    constructor(
        private pair: string,
        private from: string,
        private to: string,
        private ratesRepository: RatesRepository,
        private webSocketServer: Server
    ) {}

    getTiming(): string {
        return '*/2 * * * *'
    }

    getArguments(): any {
        return {
            pair: this.pair,
            from: this.from,
            to: this.to,
            ratesRepository: this.ratesRepository,
            webSocketServer: this.webSocketServer
        }
    }

    invoke(args: any): void {
        request
            .get(`https://www.bitstamp.net/api/v2/ticker/${args.pair}/`)
            .then(response => {
                const rate: Rate = new Rate(
                    new Currency(args.from.toLowerCase(), args.from.toLowerCase()),
                    new Currency(args.to.toLowerCase(), args.to.toLowerCase()),
                    +response.body.last,
                    +response.body.timestamp
                );

                winstonLogger.info(`Broadcast rating: ${rate.getFrom().getCode()} ${rate.getRate()}`);
                args.webSocketServer.clients.forEach((client: any) => {
                    if(client.readyState === OPEN) {
                        client.send(JSON.stringify({
                            type: "rates-update",
                            body: rate.toJSON()
                        }));
                    }
                });

                return args.ratesRepository.save(
                    new Rate(
                        new Currency(args.from.toLowerCase(), args.from.toLowerCase()),
                        new Currency(args.to.toLowerCase(), args.to.toLowerCase()),
                        +response.body.last,
                        +response.body.timestamp
                    )
                )
            })
            .catch(err => winstonLogger.error(err))
    }
}