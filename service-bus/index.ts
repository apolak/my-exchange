import dotenv = require('dotenv');
import { winstonLogger } from "../shared/logger/winston.logger";
import { Server } from "ws";

dotenv.config();

interface Subscribers {
    [event: string]: any[]
}

const socketServer: Server = new Server({ port: +process.env.WS_PORT});
const subscribers: Subscribers = {};

winstonLogger.info(`Service bus is working on ${process.env.WS_PORT}`);

const removeClientFromSubscribers = (client: any) => Object.keys(subscribers).forEach((key) => {
    subscribers[key] = subscribers[key].filter((subscriber) => subscriber !== client);
});

socketServer.on('connection', (client) => {
    winstonLogger.info(`Client connected`);

    client.on("error", (err) => {
        winstonLogger.info("Error caught: ");
        winstonLogger.info(err.stack)
    });

    client.on("close", () => {
        removeClientFromSubscribers(client);

        winstonLogger.info('Connection Lost')
    });

    client.on("message", (message: any) => {
       const data = JSON.parse(message);

       if (data.type === 'subscribe') {
           if (typeof subscribers[data.event] === "undefined") {
               subscribers[data.event] = [];
           }

           winstonLogger.info(`Client subscribed to : "${data.event}"`);
           subscribers[data.event].push(client);
       }

        if (data.type === 'publish') {
            winstonLogger.info(`Publishing message: ${data.event}`);
            if (typeof subscribers[data.event] !== "undefined") {
                winstonLogger.info(`Published message: "${data.event}"`);

                subscribers[data.event].forEach((subscriber) => subscriber.send(message));
            }
        }
    });

    client.on("end", (code, reason) => {
        removeClientFromSubscribers(client);

        winstonLogger.info('Connection Lost')
    });
});