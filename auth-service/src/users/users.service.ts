import * as request from "superagent";
import { User } from "./user.model";

export interface UsersService {
    getUserByCredentials(email: string, password: string): Promise<User>
}

export default class RestUsersService implements UsersService {
    constructor(
        private usersServiceHost: string,
        private usersServicePort: number
    ) {}

    getUserByCredentials(email: string, password: string): Promise<User> {
        return request
            .post(`${this.usersServiceHost}:${this.usersServicePort}/api/users/credentials`)
            .send({
                email,
                password
            })
            .then((value: any) => {
                console.log(value);

                return Promise.resolve({
                    id: value.body.id,
                    email: value.body.email
                })
            });
    }
}