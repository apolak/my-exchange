import Server from "./server";
import * as morgan from "morgan";
import { create } from "./routes";
import { Application } from "express";
import { AuthRoutesOptions } from "./auth/auth.routes";

const createApplication = (container: AuthRoutesOptions): Application => {
    return new Server({
        requestLogger: morgan(process.env.LOGGER_MODE || 'dev'),
        routes: create(container)
    })
        .getApp();
};

export default createApplication;
