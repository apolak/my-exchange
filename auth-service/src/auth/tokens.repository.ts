import { Token, TokenInterface } from "./token.model";
import { RedisClient } from "redis";

export default interface TokensRepository
{
    findByToken(token: string): Promise<TokenInterface>|null;
    save(token: TokenInterface): Promise<boolean>;
}

export class RedisTokenRepository implements TokensRepository {
    constructor(
        private connection: RedisClient
    ) {}

    private static getTokenKey(token:string): string {
        return `access-token:${token}`;
    }

    async findByToken(token: string): Promise<TokenInterface|null> {
        return new Promise<TokenInterface|null>((resolve, reject) => {
            this.connection.get(
                RedisTokenRepository.getTokenKey(token),
                (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    if (!result) {
                        return resolve(null);
                    }

                    const tokenData: any = JSON.parse(result);

                    return resolve(new Token(
                        tokenData.accessToken,
                        +tokenData.expiresAt,
                        tokenData.scope,
                        tokenData.userId
                    ));
                }
            );
        });
    }

    async save(token: TokenInterface): Promise<boolean> {

        return new Promise<boolean>((resolve, reject) => {
            this.connection.setex(
                RedisTokenRepository.getTokenKey(token.getToken()),
                Token.TTL,
                JSON.stringify(token.toJSON()),
                (err, result) => {
                    if (err) {
                        return reject(false);
                    }

                    return resolve(true);
                }
            );
        });
    }
}