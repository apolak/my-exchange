import { readCertificate } from '../../../shared/certificate.reader';
import jwt = require('jsonwebtoken');
import TokensRepository from "./tokens.repository";

export const validateToken = (tokensRepository: TokensRepository, token: string) => readCertificate()
    .then( cert => jwt.verify(token, cert))
    .then(() => tokensRepository.findByToken(token));
