import { Router } from 'express';
import * as express from 'express';
import { body, header } from "express-validator/check";
import TokensRepository from "./tokens.repository";
import AuthController from "./auth.controller";
import { validateToken } from './token.validator';
import { UsersService } from "../users/users.service";

export interface AuthRoutesOptions {
    tokensRepository: TokensRepository,
    usersService: UsersService
}

export const create = (options: AuthRoutesOptions) => {
    const router: Router = express.Router();
    const authController: AuthController = new AuthController(options.tokensRepository, options.usersService);

    router.post('/auth', [
        body('email').isEmail().withMessage('Invalid email address').trim().normalizeEmail(),
        body('password').isLength({min: 1}).withMessage('Missing password')
    ], authController.authenticate);
    router.get('/verify', [
        header('x-auth-token')
            .isLength({min: 1})
            .custom(value => validateToken(options.tokensRepository, value))
            .withMessage('Missing access token'),
    ], authController.verify);

    return router;
};