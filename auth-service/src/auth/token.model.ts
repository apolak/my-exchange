import jwt = require('jsonwebtoken');
import * as moment from 'moment';
import { readCertificate } from '../../../shared/certificate.reader';

export interface TokenInterface {
    getToken(): string;
    getExpiration(): number;
    getScope(): string;
    getUserId(): string;
    toJSON(): object;
}

export class Token implements TokenInterface {
    static readonly IMPERSONATE: string = 'impersonate';
    static readonly TTL: number = 7200;

    constructor(
        private token: string,
        private expiration: number,
        private scope: string,
        private userId: string
    ) {}

    static fromData(data: any): Promise<Token> {
        return readCertificate()
            .then((cert: any) => {
                return new Token(
                    jwt.sign(data, cert, { expiresIn: Token.TTL }),
                    moment().add(Token.TTL, 's').unix(),
                    data.scope,
                    data.user.id
                )
            });
    }

    getToken(): string {
        return this.token;
    }

    getExpiration(): number {
        return this.expiration;
    }

    getScope(): string {
        return this.scope;
    }

    getUserId(): string {
        return this.userId;
    }

    toJSON(): object {
        return {
            accessToken: this.token,
            expiresAt: this.expiration,
            scope: this.scope,
            userId: this.userId
        }
    }
}