import { NextFunction, Request, Response } from "express";
import { Token } from "./token.model";
import { validationResult } from "express-validator/check";
import TokensRepository from "./tokens.repository";
import { UsersService } from "../users/users.service";
import { User } from "../users/user.model";
import { ValidationError } from "../../../shared/error/validation.error";
import { HttpError } from "../../../shared/error/http.error";

export default class AuthController
{
    constructor(private tokensRepository: TokensRepository, private usersService: UsersService) {
        this.verify = this.verify.bind(this);
        this.authenticate = this.authenticate.bind(this);
    }

    verify(req: Request, res: Response, next: NextFunction): void {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            next(new ValidationError(errors.mapped()));
            return;
        }

        res
            .status(204)
            .send();
    }

    authenticate(req: Request, res: Response, next: NextFunction): void {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            next(new ValidationError(errors.mapped()));
            return;
        }

        this.usersService
            .getUserByCredentials(req.body.email, req.body.password)
            .then((user: User) => Token.fromData({
                scope: Token.IMPERSONATE,
                user:  {
                    id: user.id,
                    email: user.email
                }
            }))
            .then((token: Token) => {
                return this.tokensRepository.save(token)
                    .then((status) => {
                        if (!status) {
                            throw new HttpError('Could not save token to repostiory.', 500);
                        }

                        return res
                            .status(201)
                            .send({
                                accessToken: token.getToken(),
                                expiresAt: token.getExpiration(),
                                scope: token.getScope()
                            });
                    })
            })
            .catch(next)
    }
}