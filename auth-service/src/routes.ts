import { Router } from "express";
import * as express from "express";
import { create as createAuthRoutes, AuthRoutesOptions } from "./auth/auth.routes";

interface RoutesOptions extends AuthRoutesOptions{}

export const create = (options: RoutesOptions) => {
    const router: Router = express.Router();

    router.use('/', createAuthRoutes(options));

    return router;
};