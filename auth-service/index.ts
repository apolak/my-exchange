import redis = require('redis');
import dotenv = require('dotenv');
import { winstonLogger } from "../shared/logger/winston.logger";
import createApplication from "./src/app";
import { Application } from "express";
import { RedisClient } from "redis";
import TokensRepository, { RedisTokenRepository } from "./src/auth/tokens.repository";
import { default as RestUsersService, UsersService } from "./src/users/users.service";

dotenv.config();

const redisClient: RedisClient = redis.createClient(
    +process.env.DB_PORT,
    process.env.DB_HOST
);

winstonLogger.info('Database connected');

const tokensRepository: TokensRepository = new RedisTokenRepository(redisClient);
const usersService: UsersService = new RestUsersService(
    process.env.USERS_SERVICE_HOST,
    +process.env.USERS_SERVICE_PORT
);

const app: Application = createApplication({
    tokensRepository,
    usersService
});
const port: any = process.env.PORT || 3000;

app.listen(port);

winstonLogger.info(`Application is listening on port: ${port}`);