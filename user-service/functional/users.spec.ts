import * as request from "supertest";
import { Application } from "express";
import createApplication from "../src/app";
import { User, UserInterface } from "../src/user/user.model";
import { expect } from "chai";

const fakeRepository = {
    findById: (id: string): Promise<User> | null => {
        if (id === '1') {
            return Promise.resolve(new User(id, 'some-email@email.pl', 'some-password'));
        }

        return Promise.resolve(null);
    },
    findByCredentials: (email: string, password: string): Promise<User> | null => {
        if (email === 'some-email@email.pl' && password === 'some-password') {
            return Promise.resolve(new User('1', 'some-email@email.pl', 'some-password'));
        }

        return Promise.resolve(null);
    },
    save: (user: UserInterface): Promise<boolean> => {
        if (user.getEmail() === 'some-email@email.pl') {
            return Promise.resolve(true);
        }

        return Promise.resolve(false);
    }
};

describe('Users endpoints tests', () => {
    it('gets a auth by id', (done) => {
        const app: Application = createApplication({
            usersRepository: fakeRepository
        });

        request(app)
            .get('/api/users/1')
            .expect(200)
            .end((err: any, res: any) => {
                if (err) {
                    throw err;
                }

                expect(res.body).to.eql({
                    id: '1',
                    email: 'some-email@email.pl'
                });

                done();
            });
    });

    it('gets a auth by id', (done) => {
        const app: Application = createApplication({
            usersRepository: fakeRepository
        });

        request(app)
            .post('/api/users/credentials')
            .send({
                email: "some-email@email.pl",
                password: "some-password"
            })
            .expect(200)
            .end((err: any, res: any) => {
                if (err) {
                    throw err;
                }

                expect(res.body).to.eql({
                    id: '1',
                    email: 'some-email@email.pl'
                });

                done();
            });
    });

    it('adds a auth', (done) => {
        const userSchema = {
            title: "User schema",
            type: "object",
            required: ["id", "email"],
            properties: {
                id: {
                    type: "string",
                    pattern: "/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"
                },
                email: {
                    type: "string"
                }
            }
        };

        const app: Application = createApplication({
            usersRepository: fakeRepository
        });

        request(app)
            .post('/api/users')
            .send({
                email: "some-email@email.pl",
                password: "some-password"
            })
            .expect(201)
            .end((err: any, res: any) => {
                if (err) {
                    throw err;
                }

                expect(res.body).to.be.jsonSchema(userSchema);

                done();
            });
    });

    it('gets 404 when no auth was found', (done) => {
        const app: Application = createApplication({
            usersRepository: fakeRepository
        });

        request(app)
            .get('/api/users/unknown')
            .expect(404, done);
    });
});