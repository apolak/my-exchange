import { Router } from "express";
import * as express from "express";
import { create as createUsersRoutes, UsersRoutesOptions } from "./user/users.routes";

interface RoutesOptions extends UsersRoutesOptions{}

export const create = (options: RoutesOptions) => {
    const router: Router = express.Router();

    router.use('/users', createUsersRoutes(options));

    return router;
};