import { Router } from 'express';
import * as express from 'express';
import UsersController from "./users.controller";
import UsersRepository from "./users.repository";
import { body } from "express-validator/check";
import ServiceBusClient from "../../../shared/service-bus/service-bus.client";

export interface UsersRoutesOptions {
    usersRepository: UsersRepository,
    serviceBus: ServiceBusClient
}

export const create = (options: UsersRoutesOptions) => {
    const router: Router = express.Router();
    const usersController: UsersController = new UsersController(options.usersRepository, options.serviceBus);

    router.get('/:id', usersController.getUser);
    router.post('/credentials', [
        body('email').isEmail().withMessage('Invalid email address').trim().normalizeEmail(),
        body('password').isLength({min: 1}).withMessage('Missing password')
    ], usersController.getUserByCredentials);
    router.post('/', [
        body('email').isEmail().withMessage('Invalid email address').trim().normalizeEmail(),
        body('password').isLength({min: 1}).withMessage('Missing password')
    ], usersController.addUser);

    return router;
};