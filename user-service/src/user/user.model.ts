export interface UserInterface {
    getId(): string;
    getEmail(): string;
    getPassword(): string;
}

export class User implements UserInterface {
    constructor(
        private id: string,
        private email: string,
        private password: string
    ) {}

    getId(): string {
        return this.id;
    }

    getEmail(): string {
        return this.email;
    }

    getPassword(): string {
        return this.password;
    }
}