import { NextFunction, Request, Response } from "express";
import { User, UserInterface } from "./user.model";
import uuid = require('uuid/v4');
import { validationResult } from "express-validator/check";
import ServiceBusClient from "../../../shared/service-bus/service-bus.client";
import UsersRepository from "./users.repository";
import { USER_REGISTERED } from "./events";
import { ValidationError } from "../../../shared/error/validation.error";
import { HttpError } from "../../../shared/error/http.error";

export default class UsersController
{
    constructor(private usersRepository: UsersRepository, private serviceBus: ServiceBusClient) {
        this.getUser = this.getUser.bind(this);
        this.addUser = this.addUser.bind(this);
        this.getUserByCredentials = this.getUserByCredentials.bind(this);
    }

    getUserByCredentials(req: Request, res: Response, next: NextFunction): void {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            next(new ValidationError(errors.mapped()));
            return;
        }

        this.usersRepository.findByCredentials(req.body.email, req.body.password)
            .then((user: UserInterface|null) => {
                    if (user) {
                        return res.send({
                            id: user.getId(),
                            email: user.getEmail()
                        })
                    }

                    return res
                        .status(404)
                        .send('Not found')
                }
            )
            .catch(next)
    }

    getUser(req: Request, res: Response, next: NextFunction): void {
        this.usersRepository.findById(req.params.id)
            .then((user: UserInterface|null) => {
                    if (user) {
                        return res.send({
                            id: user.getId(),
                            email: user.getEmail()
                        })
                    }

                    return res
                        .status(404)
                        .send('Not found')
                }
            )
            .catch(next)

    }

    addUser(req: Request, res: Response, next: NextFunction): void {
        const id: string = uuid();

        const user: User = new User(
            id,
            req.body.email,
            req.body.password
        );

        this.usersRepository.findByEmail(user.getEmail())
            .then((result: any) => {
                if (result) {
                    throw new Error('User already exists');
                }

                return this.usersRepository.save(user);
            })
            .then((status) => {
                    if (!status) {
                        throw new HttpError('Could not save user to repository.', 500);
                    }

                    this.serviceBus.publish(USER_REGISTERED, {
                        userId: id
                    });

                    return res
                        .status(201)
                        .send({
                            id,
                            email: req.body.email
                        });
                }
            )
            .catch(next)
    }
}