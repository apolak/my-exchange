import { User, UserInterface } from './user.model'
import { Db, WriteOpResult } from "mongodb";

export default interface UsersRepository
{
    findByCredentials(email: string, password: string): Promise<UserInterface|null>;
    findById(id: string): Promise<UserInterface|null>;
    findByEmail(email: string): Promise<UserInterface|null>;
    save(user: UserInterface): Promise<boolean>;
}

export class MongoDBUsersRepository implements UsersRepository {
    constructor(
        private connection: Db,
        private collection: string
    ) {}

    async findByEmail(email: string): Promise<UserInterface|null> {
        return await this.connection.collection(this.collection).findOne({
            email
        })
            .then((userData: any) => {
                if (!userData) {
                    return null;
                }

                return new User(userData._id, userData.email, userData.password)
            });
    }

    async findByCredentials(email: string, password: string): Promise<UserInterface|null> {
        return await this.connection.collection(this.collection).findOne({
            email,
            password
        })
            .then((userData: any) => {
                if (!userData) {
                    return null;
                }

                return new User(userData._id, userData.email, userData.password)
            });
    }

    async findById(id: string): Promise<UserInterface|null> {
        return await this.connection.collection(this.collection).findOne({
            _id: id
        })
            .then((userData: any) => {
                if (!userData) {
                    return null;
                }

                return new User(userData._id, userData.email, userData.password)
            });
    }

    async save(user: UserInterface): Promise<boolean> {
        return await this.connection.collection(this.collection).update({
            _id: user.getId()
        } , {
            _id: user.getId(),
            email: user.getEmail(),
            password: user.getPassword()
        }, {
            upsert: true
        }).then((result: WriteOpResult) => {
            return true;
        })
    }
}