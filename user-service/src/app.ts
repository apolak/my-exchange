import Server from "./server";
import * as morgan from "morgan";
import { create } from "./routes";
import { Application } from "express";

const createApplication = (container: any): Application => {
    return new Server({
        requestLogger: morgan(process.env.LOGGER_MODE || 'dev'),
        routes: create({
            usersRepository: container.usersRepository,
            serviceBus: container.serviceBus
        })
    })
        .getApp();
};

export default createApplication;
