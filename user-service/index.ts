import mongodb = require('mongodb');
import dotenv = require('dotenv');
import { Db, MongoClient } from "mongodb";
import createApplication from "./src/app";
import { Application } from "express";
import UsersRepository, { MongoDBUsersRepository } from "./src/user/users.repository";
import { winstonLogger } from "../shared/logger/winston.logger";
import ServiceBusClient from "../shared/service-bus/service-bus.client";

dotenv.config();

const mongoClient: MongoClient = mongodb.MongoClient;
const serviceBus: ServiceBusClient = new ServiceBusClient('user_service', `ws://${process.env.SERVICE_BUS_HOST}:${process.env.SERVICE_BUS_PORT}`);

mongoClient
    .connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/users`)
    .then((db: Db) => {
        winstonLogger.info('Database connected');

        serviceBus.connect().then(() => {
            winstonLogger.info('Service bus connected');

            const usersRepository: UsersRepository = new MongoDBUsersRepository(db, 'accounts');
            const app: Application = createApplication({
                usersRepository,
                serviceBus
            });
            const port: any = process.env.PORT || 3000;

            app.listen(port);

            winstonLogger.info(`Application is listening on port: ${port}`);
        });


    })
    .catch((err) => winstonLogger.error(err));