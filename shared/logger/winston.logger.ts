import * as winston from 'winston';

const level: string = process.env.LOGGING_LEVEL || 'debug';

export const winstonLogger = new winston.Logger({
    transports: [
        new winston.transports.Console({
            level,
            timestamp: () => (new Date()).toISOString()
        })
    ]
});