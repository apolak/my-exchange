import { ServiceBusClientInterface } from "./service-bus.client";

export default class FakeServiceBusClient implements ServiceBusClientInterface {

    connect(): Promise<boolean> {
        return Promise.resolve(true);
    }

    subscribe(event: string, handler: Function) {
        return;
    }

    unsubscribe(event: string) {
        return;
    }

    publish(event: string, data: any) {
        return;
    }
}