import WebSocket = require("ws");

export interface ServiceBusClientInterface {
    connect(): Promise<boolean>;
    subscribe(event: string, handler: Function): void;
    unsubscribe(event: string): void;
    publish(event: string, data: any): void;
}

export default class ServiceBusClient implements ServiceBusClientInterface{
    private handlers: any = {};
    private socket: WebSocket;

    constructor(private serviceName: string, private url: string) {
    }

    connect(): Promise<boolean>{
        this.socket = new WebSocket(this.url);

        return new Promise((resolve, reject) => {
            this.socket.on('open', () => {
                this.socket.on('message', (data: any) => {
                    const message: any = JSON.parse(data);

                    this.handlers[message.event](message.message);
                });

                resolve(true);
            })
        })
    }

    subscribe(event: string, handler: Function) {
        this.socket.send(
            JSON.stringify({
                type: 'subscribe',
                event
            })
        );

        this.handlers[event] = handler;
    }

    unsubscribe(event: string) {
        this.socket.send(
            JSON.stringify({
                type: 'unsubscribe',
                event
            })
        );
    }

    publish(event: string, data: any) {
        this.socket.send(
            JSON.stringify({
                type: 'publish',
                event: `${this.serviceName.toUpperCase()}:${event.toUpperCase()}`,
                message: data
            })
        );
    }
}