import { NextFunction, Request, Response } from "express";
import { ValidationError } from "../error/validation.error";

export const errorHandler = (err: any, req: Request, res: Response, next: NextFunction) => {

    if (err instanceof ValidationError) {
        res
            .status(400)
            .send({
                message: err.message,
                error: err.error
            });
        return;
    }

    res
        .status(err.status || 500)
        .send({
            message: err.message,
            error: err
        });
};