import { NextFunction, Request, Response } from "express";
import jwt = require('jsonwebtoken');
import { readCertificate } from "../certificate.reader";
import { winstonLogger } from "../logger/winston.logger";

export const userIdMiddleware = (req: Request, res: Response, next: NextFunction) => {
    if (req.header('x-auth-token')) {
        readCertificate().then((cert: any) => {
            return jwt.verify(req.header('x-auth-token'), cert);
        })
            .then((decoded: any) => {
                res.locals.userId = decoded.user.id;
                next();
            })
            .catch((err) => {
                winstonLogger.error(err);
                next(err);
            })
    } else {
        next();
    }
};