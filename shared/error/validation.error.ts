export class ValidationError {
    public message: string = 'Validation error';
    constructor(public error: any){}
}