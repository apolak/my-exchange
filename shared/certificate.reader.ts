import fs = require('fs');
import path = require('path');
import util = require('util');

export const readCertificate = (): Promise<Buffer> => {
    const asyncRead = util.promisify(fs.readFile);

    return asyncRead(path.resolve(__dirname, '../jwtRS256.key'));
};