install:
	npm i --prefix ./auth-service
	npm i --prefix ./service-bus
	npm i --prefix ./currencies-service
	npm i --prefix ./finance-service
	npm i --prefix ./user-service
	npm i --prefix ./transactions-service
	npm i --prefix ./front/bff/desktop
	npm i --prefix ./front/spa
	npm i --prefix ./shared

start:
	docker-compose -f ./auth-service/docker-compose.yml up -d
	npm start --prefix ./auth-service
	npm start --prefix ./service-bus
	docker-compose -f ./currencies-service/docker-compose.yml up -d
	npm start --prefix ./currencies-service
	npm start --prefix ./finance-service
	docker-compose -f ./user-service/docker-compose.yml up -d
	npm start --prefix ./user-service
	docker-compose -f ./transactions-service/docker-compose.yml up -d
	npm start --prefix ./transactions-service
	npm start --prefix ./front/bff/desktop
	npm start --prefix ./front/spa

stop:
	docker-compose -f ./auth-service/docker-compose.yml down
	npm run stop --prefix ./auth-service
	npm run stop --prefix ./service-bus
	docker-compose -f ./currencies-service/docker-compose.yml down
	npm run stop --prefix ./currencies-service
	npm run stop --prefix ./finance-service
	docker-compose -f ./user-service/docker-compose.yml down
	npm run stop --prefix ./user-service
	docker-compose -f ./transactions-service/docker-compose.yml down
	npm run stop --prefix ./transactions-service
	npm run stop --prefix ./front/bff/desktop
