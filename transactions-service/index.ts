import mongodb = require('mongodb');
import dotenv = require('dotenv');
import { Db, MongoClient } from "mongodb";
import EventsStore, { MongoDBEventsStore } from "./src/events/events.repository";
import CommandBus from "./src/command-bus/command-bus";
import CreateAccountHandler from "./src/account/create-account/create-account.handler";
import { winstonLogger } from "../shared/logger/winston.logger";
import ServiceBusClient from "../shared/service-bus/service-bus.client";
import {
    USER_REGISTERED, WITHDRAW_CANCELED_BY_ADMIN,
    WITHDRAW_FINISHED_BY_ADMIN
} from "../shared/service-bus/events";
import CreateAccountCommand from "./src/account/create-account/create-account.command";
import DepositMoneyHandler from "./src/account/deposit-money/deposit-money.handler";
import { Application } from "express";
import createApplication from "./src/app";
import MoneyWithdrawIssuedHandler from "./src/account/money-withdraw/money-withdraw-issued.handler";
import MoneyWithdrawCanceledCommand from "./src/account/money-withdraw/money-withdraw-canceled.command";
import MoneyWithdrawFinishedCommand from "./src/account/money-withdraw/money-withdraw-finished.command";
import MoneyWithdrawCanceledHandler from "./src/account/money-withdraw/money-withdraw-canceled.handler";
import MoneyWithdrawFinishedHandler from "./src/account/money-withdraw/money-withdraw-finished.handler";
import CreateOrderHandler from "./src/account/order/create-order.handler";
import CancelOrderHandler from "./src/account/order/cancel-order.handler";
import MatchTransactionsHandler from "./src/account/transaction/match-transactions.handler";
import { MongoDBActiveOrdersRepository } from "./src/account/order/active-orders.repository";
import MatchOrderSaga from "./src/sagas/match-order.saga";
import { MongoDBTransactionsRepository } from "./src/account/transaction/transactions.repository";
import TransactionsProjector from "./src/projectors/transactions.projector";
import ReadSocketServer from "./src/read-socket.server";
import WalletProjector from "./src/projectors/wallet.projector";
import InMemoryWalletRepository, { WalletRepository } from "./src/account/wallet/in-memory-wallet.repository";

dotenv.config();

const mongoClient: MongoClient = mongodb.MongoClient;
const serviceBus: ServiceBusClient = new ServiceBusClient('transactions_service', `ws://${process.env.SERVICE_BUS_HOST}:${process.env.SERVICE_BUS_PORT}`);

mongoClient
    .connect(`mongodb://${process.env.DB_TRANSACTIONS_HOST}:${process.env.DB_TRANSACTIONS_PORT}/transactions`)
    .then((db: Db) => {
        winstonLogger.info('Database connected');
        serviceBus.connect().then(() => {
            winstonLogger.info('Service bus connected');

            const eventsStore: EventsStore = new MongoDBEventsStore(db, 'events', serviceBus);
            const activeOrdersRepository: MongoDBActiveOrdersRepository = new MongoDBActiveOrdersRepository(db);
            const transactionsRepository: MongoDBTransactionsRepository = new MongoDBTransactionsRepository(db);
            const walletRepository: WalletRepository = new InMemoryWalletRepository();

            const readModelSocket: ReadSocketServer = new ReadSocketServer(
                +process.env.SOCKET_PORT,
                transactionsRepository,
                activeOrdersRepository,
                walletRepository,
                3000
            );

            readModelSocket.listen();

            const commandBus: CommandBus = new CommandBus(winstonLogger);
            commandBus.addHandler(new CreateAccountHandler(eventsStore));
            commandBus.addHandler(new DepositMoneyHandler(eventsStore));
            commandBus.addHandler(new MoneyWithdrawIssuedHandler(eventsStore));
            commandBus.addHandler(new MoneyWithdrawCanceledHandler(eventsStore));
            commandBus.addHandler(new MoneyWithdrawFinishedHandler(eventsStore));
            commandBus.addHandler(new CreateOrderHandler(eventsStore));
            commandBus.addHandler(new CancelOrderHandler(eventsStore));
            commandBus.addHandler(new MatchTransactionsHandler(eventsStore, activeOrdersRepository));

            eventsStore.addSaga(new MatchOrderSaga(activeOrdersRepository, commandBus, readModelSocket));
            eventsStore.addProjector(new TransactionsProjector(transactionsRepository));
            eventsStore.addProjector(new WalletProjector(walletRepository, readModelSocket));

            const app: Application = createApplication({
                commandBus,
                serviceBus
            });
            const port: any = process.env.PORT || 3000;

            app.listen(port);

            serviceBus.subscribe(USER_REGISTERED, (message: any) => {
                const command: CreateAccountCommand = new CreateAccountCommand(message.userId);

                commandBus.handle(command);
            });

            serviceBus.subscribe(WITHDRAW_CANCELED_BY_ADMIN, (message: any) => {
                const command: MoneyWithdrawCanceledCommand = new MoneyWithdrawCanceledCommand(message.userId, message.lockId, message.issuer);

                commandBus.handle(command);
            });

            serviceBus.subscribe(WITHDRAW_FINISHED_BY_ADMIN, (message: any) => {
                const command: MoneyWithdrawFinishedCommand = new MoneyWithdrawFinishedCommand(message.userId, message.lockId, message.issuer);

                commandBus.handle(command);
            });
        });
    })
    .catch((err: any) => winstonLogger.error(err));