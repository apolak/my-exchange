import { Transaction } from "./account/transaction/transaction";
import { ReadSocket } from "./read-socket.server";

export default class FakeReadSocketServer implements ReadSocket{
    listen(): void{
        return;
    }

    publishUserWallets(userId: string, wallets: any) {
        return;
    }

    publishNewTransaction(transaction: Transaction) {
        return;
    }
}