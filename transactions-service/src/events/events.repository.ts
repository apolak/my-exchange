import { Db, WriteOpResult } from "mongodb";
import { Event } from "../event-sourcing/event.interface";
import EventsFactory from "../account/events/events.factory";
import { ServiceBusClientInterface } from "../../../shared/service-bus/service-bus.client";
import Projector from "../projectors/projector";
import Saga from "../sagas/saga";

export default interface EventsStore
{
    findAll(): Promise<Event[]>;
    findByAggregateId(aggregateId: string): Promise<Event[]>;
    save(event: Event): Promise<boolean>;
    addProjector(projector: Projector): void;
    addSaga(saga: Saga): void;
}

export class MongoDBEventsStore implements EventsStore {
    private projectors: Projector[] = [];
    private sagas: Saga[] = [];

    constructor(
        private connection: Db,
        private collection: string,
        private serviceBus: ServiceBusClientInterface
    ) {}

    addProjector(projector: Projector): void {
        this.projectors.push(projector);
    }

    addSaga(saga: Saga): void {
        this.sagas.push(saga);
    }

    async findAll(): Promise<Event[]> {
        return await this.connection.collection(this.collection)
            .find()
            .toArray()
            .then(events => {
                return events.map(event => EventsFactory.create(event))
            });
    }

    async findByAggregateId(aggregateId: string): Promise<Event[]> {
        return await this.connection.collection(this.collection)
            .find({
                aggregateId
            })
            .sort({
                date: 1
            })
            .toArray()
            .then((events: any) => {
                return events.map((event: any) => EventsFactory.create(event))
            })
    }

    async save(event: Event): Promise<boolean> {
        return await this.connection.collection(this.collection).insert({
            type: event.getType(),
            date: event.getDate(),
            aggregateId: event.getAggregateId(),
            version: event.getVersion(),
            data: event.getData()
        }).then((result: WriteOpResult) => {
            this.projectors.forEach((projector: Projector) => projector.project(event));
            this.sagas.forEach((saga: Saga) => saga.execute(event));

            this.serviceBus.publish(event.getType(), event.toJSON());

            return true;
        })
    }
}