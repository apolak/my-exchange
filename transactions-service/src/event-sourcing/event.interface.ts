export interface Event {
    getType(): string;
    getVersion(): number;
    getAggregateId(): string;
    getDate(): number;
    getData(): any;
    toJSON(): any;
}