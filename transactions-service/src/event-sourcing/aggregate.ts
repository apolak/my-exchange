import { Event } from "./event.interface";

export default interface AggregateRoot {
    getRecordedEvents(): Event[];
}