import { Event } from "../event-sourcing/event.interface";
import Saga from "./saga";
import ActiveOrdersRepository from "../account/order/active-orders.repository";
import CommandBus from "../command-bus/command-bus";
import BuyOrderCreatedEvent from "../account/events/buy-order-created.event";
import Order from "../account/order/order";
import SellOrderCreatedEvent from "../account/events/sell-order-created.event";
import TransactionEvent from "../account/events/transaction.event";
import OrderCanceledEvent from "../account/events/order-canceled.event";
import MatchTransactionsCommand from "../account/transaction/match-transactions.command";
import OrderFinishedEvent from "../account/events/order-finished.event";
import ReadSocketServer from "../read-socket.server";
import { createFromEventData } from "../account/transaction/transaction";

export default class MatchOrderSaga implements Saga {
    constructor(
        private activeOrdersRepository: ActiveOrdersRepository,
        private commandBus: CommandBus,
        private transactionSocket: ReadSocketServer
    ) {}

    execute(event: Event): void {
        switch(event.getType()) {
            case BuyOrderCreatedEvent.type:
                this.activeOrdersRepository.add({
                    _id: event.getData().id,
                    type: Order.BUY,
                    unitPrice: event.getData().unitPrice,
                    pair: event.getData().pair,
                    amount: event.getData().amount,
                    left: event.getData().amount,
                    userId: event.getAggregateId()
                }).then(() => {
                    const command: MatchTransactionsCommand = new MatchTransactionsCommand(event.getData().id);

                    this.commandBus.handle(command);
                });
                break;
            case SellOrderCreatedEvent.type:
                this.activeOrdersRepository.add({
                    _id: event.getData().id,
                    type: Order.SELL,
                    unitPrice: event.getData().unitPrice,
                    pair: event.getData().pair,
                    amount: event.getData().amount,
                    left: event.getData().amount,
                    userId: event.getAggregateId()
                }).then(() => {
                    const command: MatchTransactionsCommand = new MatchTransactionsCommand(event.getData().id);

                    this.commandBus.handle(command);
                });
                break;
            case TransactionEvent.type:
                this.activeOrdersRepository.update(event.getData().orderId, event.getData().orderTotalAfter)
                    .then(() => this.transactionSocket.publishNewTransaction(createFromEventData(<TransactionEvent>event)));
                break;
            case OrderFinishedEvent.type:
                this.activeOrdersRepository.remove(event.getData().orderId);
                break;
            case OrderCanceledEvent.type:
                this.activeOrdersRepository.remove(event.getData().orderId);
                break;
        }
    }
}