import { Event } from "../event-sourcing/event.interface";

export default interface Saga {
    execute(event: Event): void;
}