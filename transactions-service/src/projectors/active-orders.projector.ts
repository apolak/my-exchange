import Projector from "./projector";
import ActiveOrdersRepository from "../account/order/active-orders.repository";
import BuyOrderCreatedEvent from "../account/events/buy-order-created.event";
import SellOrderCreatedEvent from "../account/events/sell-order-created.event";
import Order from "../account/order/order";
import TransactionEvent from "../account/events/transaction.event";
import OrderFinishedEvent from "../account/events/order-finished.event";
import OrderCanceledEvent from "../account/events/order-canceled.event";
import { Event } from "../event-sourcing/event.interface";

export default class ActiveOrdersProjector implements Projector {
    constructor(
        private activeOrdersRepository: ActiveOrdersRepository
    ){}

    project(event: Event): Promise<any> {
        switch(event.getType()) {
            case BuyOrderCreatedEvent.type:
                return this.activeOrdersRepository.add({
                    _id: event.getData().id,
                    type: Order.BUY,
                    unitPrice: event.getData().unitPrice,
                    pair: event.getData().pair,
                    amount: event.getData().amount,
                    left: event.getData().amount,
                    userId: event.getAggregateId()
                });
            case SellOrderCreatedEvent.type:
                return this.activeOrdersRepository.add({
                    _id: event.getData().id,
                    type: Order.SELL,
                    unitPrice: event.getData().unitPrice,
                    pair: event.getData().pair,
                    amount: event.getData().amount,
                    left: event.getData().amount,
                    userId: event.getAggregateId()
                });
            case TransactionEvent.type:
                return this.activeOrdersRepository.update(event.getData().orderId, event.getData().orderTotalAfter);
            case OrderFinishedEvent.type:
                return this.activeOrdersRepository.remove(event.getData().orderId);
            case OrderCanceledEvent.type:
                return this.activeOrdersRepository.remove(event.getData().orderId);
            default:
                return Promise.resolve();
        }
    }

    async rebuild(events: Event[]): Promise<any> {
        this.activeOrdersRepository.clear();

        for (const event of events) {
            await this.project(event);
        }

        Promise.resolve();
    }
}