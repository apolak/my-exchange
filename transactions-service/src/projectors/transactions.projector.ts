import Projector from "./projector";
import TransactionEvent from "../account/events/transaction.event";
import { Event } from "../event-sourcing/event.interface";
import TransactionsRepository from "../account/transaction/transactions.repository";
import { createFromEventData } from "../account/transaction/transaction";

export default class TransactionsProjector implements Projector {
    constructor(
        private transactionsRepository: TransactionsRepository
    ){}

    project(event: Event): Promise<any> {
        switch(event.getType()) {
            case TransactionEvent.type:
                return this.transactionsRepository.add(createFromEventData(<TransactionEvent>event))
        }
    }

    async rebuild(events: Event[]): Promise<any> {
        this.transactionsRepository.clear();

        for (const event of events) {
            await this.project(event);
        }

        Promise.resolve();
    }
}