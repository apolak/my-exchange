import { Event } from "../event-sourcing/event.interface";

export default interface Projector {
    project(event: Event): Promise<any>;
    rebuild(events: Event[]): Promise<boolean>;
}