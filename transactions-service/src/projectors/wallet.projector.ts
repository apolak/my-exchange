import Projector from "./projector";
import { Event } from "../event-sourcing/event.interface";
import WalletAddedEvent from "../account/events/wallet-added.event";
import { WalletRepository } from "../account/wallet/in-memory-wallet.repository";
import MoneyDepositedEvent from "../account/events/money-deposited.event";
import MoneyWithdrawIssuedEvent from "../account/events/money-withdraw-issued.event";
import MoneyWithdrawCanceledEvent from "../account/events/money-withdraw-canceled.event";
import AdminCanceledWithdrawEvent from "../account/events/admin-canceled-withdraw.event";
import MoneyWithdrawFinishedEvent from "../account/events/money-withdraw-finished.event";
import BuyOrderCreatedEvent from "../account/events/buy-order-created.event";
import SellOrderCreatedEvent from "../account/events/sell-order-created.event";
import OrderCanceledEvent from "../account/events/order-canceled.event";
import Order from "../account/order/order";
import { ReadSocket } from "../read-socket.server";
import TransactionEvent from "../account/events/transaction.event";

export default class WalletProjector implements Projector {
    private publishChanges: boolean = true;

    constructor(
        private walletRepository: WalletRepository,
        private readSocket: ReadSocket
    ) {}

    project(event: Event): Promise<any> {
        switch(event.getType()) {
            case WalletAddedEvent.type:
                return this.walletRepository.add(event.getAggregateId(), event.getData().currency)
                    .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                        this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                    }));
            case MoneyDepositedEvent.type:
                return this.walletRepository.updateWalletAvailableAmount(event.getAggregateId(), event.getData().currency, +event.getData().amount)
                    .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                        this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                    }));
            case MoneyWithdrawIssuedEvent.type:
                return this.walletRepository.updateWalletAvailableAmount(event.getAggregateId(), event.getData().currency, -event.getData().amount)
                    .then(() => this.walletRepository.updateWalletLockedAmount(event.getAggregateId(), event.getData().currency, +event.getData().amount))
                    .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                        this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                    }));
            case AdminCanceledWithdrawEvent.type:
            case MoneyWithdrawCanceledEvent.type:
                return this.walletRepository.updateWalletAvailableAmount(event.getAggregateId(), event.getData().currency, +event.getData().amount)
                    .then(() => this.walletRepository.updateWalletLockedAmount(event.getAggregateId(), event.getData().currency, -event.getData().amount))
                    .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                        this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                    }));
            case MoneyWithdrawFinishedEvent.type:
                return this.walletRepository.updateWalletLockedAmount(event.getAggregateId(), event.getData().currency, -event.getData().amount)
                    .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                        this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                    }));
            case BuyOrderCreatedEvent.type:
                return this.walletRepository.updateWalletLockedAmount(event.getAggregateId(), event.getData().pair.second, +(event.getData().unitPrice * event.getData().amount))
                    .then(() => this.walletRepository.updateWalletAvailableAmount(event.getAggregateId(), event.getData().pair.second, -(event.getData().unitPrice * event.getData().amount)))
                    .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                        this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                    }));
            case SellOrderCreatedEvent.type:
                return this.walletRepository.updateWalletLockedAmount(event.getAggregateId(), event.getData().pair.first, +event.getData().amount)
                    .then(() => this.walletRepository.updateWalletAvailableAmount(event.getAggregateId(), event.getData().pair.first, -event.getData().amount))
                    .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                        this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                    }));
            case OrderCanceledEvent.type:
                if (event.getData().type === Order.SELL) {
                    return this.walletRepository.updateWalletLockedAmount(event.getAggregateId(), event.getData().pair.first, -(event.getData().amount))
                        .then(() => this.walletRepository.updateWalletAvailableAmount(event.getAggregateId(), event.getData().pair.first, +(event.getData().amount)))
                        .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                            this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                        }));
                }

                return this.walletRepository.updateWalletLockedAmount(event.getAggregateId(), event.getData().pair.second, -(event.getData().amount * event.getData().unitPrice))
                    .then(() => this.walletRepository.updateWalletAvailableAmount(event.getAggregateId(), event.getData().pair.second, +(event.getData().amount * event.getData().unitPrice)))
                    .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                        this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                    }));
            case TransactionEvent.type:
                if(event.getData().type === Order.SELL) {
                    return this.walletRepository.updateWalletLockedAmount(event.getAggregateId(), event.getData().pair.first, -(event.getData().amount - event.getData().amountLeft))
                        .then(() => this.walletRepository.updateWalletAvailableAmount(event.getAggregateId(), event.getData().pair.second, +((event.getData().amount - event.getData().amountLeft) * event.getData().unitPrice)))
                        .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                            this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                        }));
                }

                return this.walletRepository.updateWalletLockedAmount(event.getAggregateId(), event.getData().pair.second, -(event.getData().amount - event.getData().amountLeft) * event.getData().unitPrice)
                    .then(() => this.walletRepository.updateWalletAvailableAmount(event.getAggregateId(), event.getData().pair.first, +((event.getData().amount - event.getData().amountLeft))))
                    .then(() => this.walletRepository.findByUserId(event.getAggregateId()).then(wallets => {
                        this.readSocket.publishUserWallets(event.getAggregateId(), wallets);
                    }));
        }
    }

    async rebuild(events: Event[]): Promise<any> {
        this.walletRepository.clear();
        this.publishChanges = false;

        for (const event of events) {
            await this.project(event);
        }

        this.publishChanges = true;

        Promise.resolve();
    }
}