import { Router } from "express";
import * as express from "express";
import { create as createAccountsRoutes, AccountsRoutesOptions } from "./account/accounts.routes";

interface RoutesOptions extends AccountsRoutesOptions{}

export const create = (options: RoutesOptions) => {
    const router: Router = express.Router();

    router.use('/accounts', createAccountsRoutes(options));

    return router;
};