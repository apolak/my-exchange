import mongodb = require('mongodb');
import dotenv = require('dotenv');
import { Db, MongoClient } from "mongodb";
import { winstonLogger } from "../../../shared/logger/winston.logger";
import { default as EventsStore, MongoDBEventsStore } from "../events/events.repository";
import { ServiceBusClientInterface } from "../../../shared/service-bus/service-bus.client";
import { Event } from "../event-sourcing/event.interface";
import ActiveOrdersProjector from "../projectors/active-orders.projector";
import { MongoDBActiveOrdersRepository } from "../account/order/active-orders.repository";
import FakeServiceBusClient from "../../../shared/service-bus/fake-service.bus.client";

dotenv.config();

const mongoClient: MongoClient = mongodb.MongoClient;
const serviceBus: ServiceBusClientInterface = new FakeServiceBusClient();

mongoClient
    .connect(`mongodb://${process.env.DB_TRANSACTIONS_HOST}:${process.env.DB_TRANSACTIONS_PORT}/transactions`)
    .then((db: Db) => {
        winstonLogger.info('Database connected');

        const eventsStore: EventsStore = new MongoDBEventsStore(db, 'events', serviceBus);
        const activeOrdersProjector: ActiveOrdersProjector = new ActiveOrdersProjector(new MongoDBActiveOrdersRepository(db));

        eventsStore.findAll().then((events: Event[]) => {
            activeOrdersProjector.rebuild(events).then(() => process.exit())
        });
    })
    .catch((err: any) => winstonLogger.error(err));