import mongodb = require('mongodb');
import dotenv = require('dotenv');
import { Db, MongoClient } from "mongodb";
import { winstonLogger } from "../../../shared/logger/winston.logger";
import { ServiceBusClientInterface } from "../../../shared/service-bus/service-bus.client";
import { Event } from "../event-sourcing/event.interface";
import WalletProjector from "../projectors/wallet.projector";
import FakeServiceBusClient from "../../../shared/service-bus/fake-service.bus.client";
import EventsStore, { MongoDBEventsStore } from "../events/events.repository";
import InMemoryWalletRepository from "../account/wallet/in-memory-wallet.repository";
import { ReadSocket } from "../read-socket.server";
import FakeReadSocketServer from "../fake-read-socket.server";

dotenv.config();

const mongoClient: MongoClient = mongodb.MongoClient;
const serviceBus: ServiceBusClientInterface = new FakeServiceBusClient();
const readSocket: ReadSocket = new FakeReadSocketServer();

mongoClient
    .connect(`mongodb://${process.env.DB_TRANSACTIONS_HOST}:${process.env.DB_TRANSACTIONS_PORT}/transactions`)
    .then((db: Db) => {
        winstonLogger.info('Database connected');

        const eventsStore: EventsStore = new MongoDBEventsStore(db, 'events', serviceBus);
        const walletProjector: WalletProjector = new WalletProjector(new InMemoryWalletRepository(), readSocket);

        eventsStore.findAll().then((events: Event[]) => {
            walletProjector.rebuild(events).then(() => process.exit())
        });
    })
    .catch((err: any) => winstonLogger.error(err));