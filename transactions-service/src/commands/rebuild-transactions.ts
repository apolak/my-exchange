import mongodb = require('mongodb');
import dotenv = require('dotenv');
import { Db, MongoClient } from "mongodb";
import { winstonLogger } from "../../../shared/logger/winston.logger";
import { default as EventsStore, MongoDBEventsStore } from "../events/events.repository";
import { ServiceBusClientInterface } from "../../../shared/service-bus/service-bus.client";
import TransactionsProjector from "../projectors/transactions.projector";
import { MongoDBTransactionsRepository } from "../account/transaction/transactions.repository";
import { Event } from "../event-sourcing/event.interface";
import FakeServiceBusClient from "../../../shared/service-bus/fake-service.bus.client";

dotenv.config();

const mongoClient: MongoClient = mongodb.MongoClient;
const serviceBus: ServiceBusClientInterface = new FakeServiceBusClient();

mongoClient
    .connect(`mongodb://${process.env.DB_TRANSACTIONS_HOST}:${process.env.DB_TRANSACTIONS_PORT}/transactions`)
    .then((db: Db) => {
        winstonLogger.info('Database connected');

        const eventsStore: EventsStore = new MongoDBEventsStore(db, 'events', serviceBus);
        const transactionsProjector: TransactionsProjector = new TransactionsProjector(new MongoDBTransactionsRepository(db));

        eventsStore.findAll().then((events: Event[]) => {
            transactionsProjector.rebuild(events).then(() => process.exit())
        });
    })
    .catch((err: any) => winstonLogger.error(err));