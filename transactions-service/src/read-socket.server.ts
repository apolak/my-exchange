import WebSocket = require("ws");
import { winstonLogger } from "../../shared/logger/winston.logger";
import TransactionsRepository from "./account/transaction/transactions.repository";
import { Transaction } from "./account/transaction/transaction";
import ActiveOrdersRepository from "./account/order/active-orders.repository";
import { OPEN } from "ws";
import urlParser = require('url');
import jwt = require('jsonwebtoken');
import { WalletRepository } from "./account/wallet/in-memory-wallet.repository";

export interface ReadSocket {
    listen(): void;
    publishUserWallets(userId: string, wallets: any): void;
    publishNewTransaction(transaction: Transaction): void;
}

export default class ReadSocketServer {
    private server: WebSocket.Server;

    private transactionsToBePublished: Transaction[] = [];
    private lastOrders: any = {};

    constructor(
        port: number,
        private transactionsRepository: TransactionsRepository,
        private activeOrdersRepository: ActiveOrdersRepository,
        private walletRepository: WalletRepository,
        private latestTransactionsInterval: number
    ) {
        this.server = new WebSocket.Server({
            port
        });

        winstonLogger.info(`Socket works on port: ${port}`);
    }

    listen(): void{
        this.server.on('connection', (client: any, req) => {
            const url: any = urlParser.parse(req.url, true);
            const decodedToken: any = jwt.decode(url.query.accessToken);
            client.userId = decodedToken.user.id;

            client.on("error", (err: any) => {
                winstonLogger.info("Error caught: ");
                winstonLogger.info(err.stack)
            });

            client.on("end", (code: any, reason: any) => {
                winstonLogger.info('Connection Lost')
            });

            this.walletRepository.findByUserId(client.userId).then(wallets => {
                client.send(JSON.stringify({
                    type: "wallets",
                    body: wallets
                }))
            });

            this.transactionsRepository.findLatestTransactions(20).then((transactions) => {
                client.send(JSON.stringify({
                    type: "latest-transactions",
                    body: transactions
                }))
            });

            this.activeOrdersRepository.findOrders(20).then((orders) => {
                this.lastOrders = JSON.stringify(orders);

                client.send(JSON.stringify({
                    type: "latest-orders",
                    body: orders
                }))
            });
        });

        setInterval(() => {
            if (this.transactionsToBePublished.length > 0) {
                this.server.clients.forEach((client) => {
                    //console.log(client.myCustomVariable);
                    if (client.readyState === OPEN) {
                        client.send(JSON.stringify({
                            type: "new-transactions",
                            body: this.transactionsToBePublished
                        }))
                    }
                });
            }

            this.transactionsToBePublished = [];

            this.activeOrdersRepository.findOrders(20).then((orders) => {
                const stringifiedOrders: string = JSON.stringify(orders);
                if (this.lastOrders !== stringifiedOrders) {
                    this.lastOrders = stringifiedOrders;

                    this.server.clients.forEach(client => {
                        if (client.readyState === OPEN) {
                            client.send(JSON.stringify({
                                type: "latest-orders",
                                body: orders
                            }))
                        }
                    })
                }
            });
        }, this.latestTransactionsInterval);
    }

    publishUserWallets(userId: string, wallets: any) {
        this.server.clients.forEach((client: any) => {
            if (client.readyState === OPEN && client.userId === userId) {
                client.send(JSON.stringify({
                    type: "wallets",
                    body: wallets
                }))
            }
        })
    }

    publishNewTransaction(transaction: Transaction) {
        this.transactionsToBePublished.push(transaction);
    }
}