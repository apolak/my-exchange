import { LoggerInstance } from "winston";
import CommandResult from "./command-result";

export interface Command {
    getType(): string;
}

export interface Handler {
    handle(command: Command): Promise<CommandResult>;
    getType(): string
}

export default class CommandBus {
    private handlers: {
        [key: string]: Handler
    } = {};

    constructor(private logger: LoggerInstance) {}

    addHandler(handler: Handler) {
        this.handlers[handler.getType()] = handler;
    }

    handle(command: Command): Promise<CommandResult> {
        if (typeof this.handlers[command.getType()] === 'undefined') {
            throw new Error(`Could not find handler for command "${command.getType()}"`);
        }

        return this.handlers[command.getType()]
            .handle(command)
            .then(() => CommandResult.success())
            .catch((err: any) => {
                this.logger.error(err);

                throw err;
            });
    }
}