export default class CommandResult {
    static SUCCESS: string = 'success';
    static FAILURE: string = 'failure';

    private error: any;

    constructor(private status: string) {}

    static success() {
        return new CommandResult(CommandResult.SUCCESS);
    }

    static failure(error: any) {
        const command: CommandResult =  new CommandResult(CommandResult.FAILURE);
        command.setError(error);

        return command;
    }

    isSuccess(): boolean {
        return this.status === CommandResult.SUCCESS;
    }

    setError(error: any) {
        this.error = error;
    }

    getError(): any {
        return this.error;
    }
}