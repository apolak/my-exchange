import { Handler } from "../../command-bus/command-bus";
import EventsStore from "../../events/events.repository";
import { Event } from "../../event-sourcing/event.interface";
import Account from "../account";
import MoneyWithdrawFinishedCommand from "./money-withdraw-finished.command";

export default class MoneyWithdrawFinishedHandler implements Handler {
    constructor(private eventsStore: EventsStore) {}

    handle(command: MoneyWithdrawFinishedCommand): Promise<any> {
        return this.eventsStore.findByAggregateId(command.userId)
            .then((events: Event[]) => {
                if (events.length <= 0) {
                    throw new Error(`User with id "${command.userId}" does not exists`);
                }

                const account: Account = Account.fromEvents(command.userId, events);

                account.withdraw(
                    command.withdrawId,
                    command.issuer
                );

                account.getRecordedEvents().forEach((event: Event) => this.eventsStore.save(event));
            });
    }

    getType(): string {
        return MoneyWithdrawFinishedCommand.type;
    }
}