import { Command } from "../../command-bus/command-bus";

export default class MoneyWithdrawFinishedCommand implements Command{
    static type: string = 'MoneyWithdrawFinishedCommand';

    constructor(
        public userId: string,
        public withdrawId: string,
        public issuer: string
    ){}

    getType(): string {
        return MoneyWithdrawFinishedCommand.type;
    }
}