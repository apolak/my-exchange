import { Handler } from "../../command-bus/command-bus";
import EventsStore from "../../events/events.repository";
import { Event } from "../../event-sourcing/event.interface";
import Account from "../account";
import MoneyWithdrawIssuedCommand from "./money-withdraw-issued.command";

export default class MoneyWithdrawIssuedHandler implements Handler {
    constructor(private eventsStore: EventsStore) {}

    handle(command: MoneyWithdrawIssuedCommand): Promise<any> {
        return this.eventsStore.findByAggregateId(command.userId)
            .then((events: Event[]) => {
                if (events.length <= 0) {
                    throw new Error(`User with id "${command.userId}" does not exists`);
                }

                const account: Account = Account.fromEvents(command.userId, events);

                account.issueWithdraw(
                    command.currency.toLowerCase(),
                    command.amount
                );

                account.getRecordedEvents().forEach((event: Event) => this.eventsStore.save(event));
            });
    }

    getType(): string {
        return MoneyWithdrawIssuedCommand.type;
    }
}