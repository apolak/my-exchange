import { Command } from "../../command-bus/command-bus";

export default class MoneyWithdrawIssuedCommand implements Command{
    static type: string = 'MoneyWithdrawIssuedCommand';

    constructor(
        public userId: string,
        public currency: string,
        public amount: number
    ){}

    getType(): string {
        return MoneyWithdrawIssuedCommand.type;
    }
}