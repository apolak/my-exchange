import { Command } from "../../command-bus/command-bus";

export default class MoneyWithdrawCanceledCommand implements Command{
    static type: string = 'MoneyWithdrawCanceledCommand';

    constructor(
        public userId: string,
        public withdrawId: string,
        public issuer: string
    ){}

    getType(): string {
        return MoneyWithdrawCanceledCommand.type;
    }
}