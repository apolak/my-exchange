import { Command } from "../../command-bus/command-bus";

export default class DepositMoneyCommand implements Command{
    static type: string = 'DepositMoneyCommand';

    constructor(
        public userId: string,
        public currency: string,
        public amount: number
    ){}

    getType(): string {
        return DepositMoneyCommand.type;
    }
}