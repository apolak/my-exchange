import { Router } from 'express';
import * as express from 'express';
import { body, param } from "express-validator/check";
import ServiceBusClient from "../../../shared/service-bus/service-bus.client";
import CommandBus from "../../../transactions-service/src/command-bus/command-bus";
import AccountsController from "./accounts.controller";

export interface AccountsRoutesOptions {
    commandBud: CommandBus,
    serviceBus: ServiceBusClient
}

export const create = (options: AccountsRoutesOptions) => {
    const router: Router = express.Router();
    const accountsController: AccountsController = new AccountsController(options.commandBud);

    router.post('/deposit', [
        body('currency').isAlpha().withMessage('Invalid currency code').trim(),
        body('amount').isFloat().withMessage('Invalid amount')
    ], accountsController.deposit);

    router.delete('/withdraw/:id', [
        param('id').isLength({min: 1}).withMessage('Invalid withdraw id').trim()
    ], accountsController.cancelWithdraw);

    router.post('/withdraw', [
        body('currency').isAlpha().withMessage('Invalid currency code').trim(),
        body('amount').isFloat().withMessage('Invalid amount')
    ], accountsController.withdraw);

    router.post('/order', [
        body('pair.first').exists().withMessage('Invalid currency code').trim(),
        body('pair.second').exists().withMessage('Invalid currency code').trim(),
        body('unitPrice').isFloat().withMessage('Invalid amount'),
        body('amount').isFloat().withMessage('Invalid amount'),
        body('type').exists().withMessage('Invalid order type')
    ], accountsController.issueOrder);

    router.delete('/order/:id', [
        param('id').isLength({min: 1}).withMessage('Invalid order id').trim()
    ], accountsController.cancelOrder);

    return router;
};