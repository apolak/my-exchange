import { Request, Response, NextFunction } from "express";
import { validationResult } from "express-validator/check";
import CommandBus from "../../../transactions-service/src/command-bus/command-bus";
import DepositMoneyCommand from ".//deposit-money/deposit-money.command";
import MoneyWithdrawIssuedCommand from "./money-withdraw/money-withdraw-issued.command";
import CommandResult from "../command-bus/command-result";
import MoneyWithdrawCanceledCommand from "./money-withdraw/money-withdraw-canceled.command";
import CreateOrderCommand from "./order/create-order.command";
import CancelOrderCommand from "./order/cancel-order.command";
import { ValidationError } from "../../../shared/error/validation.error";

export default class UsersController
{
    constructor(private commandBus: CommandBus) {
        this.deposit = this.deposit.bind(this);
        this.withdraw = this.withdraw.bind(this);
        this.cancelWithdraw = this.cancelWithdraw.bind(this);
        this.issueOrder = this.issueOrder.bind(this);
        this.cancelOrder = this.cancelOrder.bind(this);
    }

    deposit(req: Request, res: Response, next: NextFunction): void {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            next(new ValidationError(errors.mapped()));
            return;
        }

        const command: DepositMoneyCommand = new DepositMoneyCommand(
            res.locals.userId,
            req.body.currency,
            req.body.amount
        );

        this.commandBus
            .handle(command)
            .then(() => {
                res
                    .status(202)
                    .send();
            })
            .catch(next);
    }

    cancelWithdraw(req: Request, res: Response, next: NextFunction): void {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            next(new ValidationError(errors.mapped()));
            return;
        }

        const command: MoneyWithdrawCanceledCommand = new MoneyWithdrawCanceledCommand(
            res.locals.userId,
            req.params.id,
            res.locals.userId
        );

        this.commandBus.handle(command)
            .then((result: CommandResult) => {
                    res
                        .status(200)
                        .send();
            })
            .catch(next);
    }

    withdraw(req: Request, res: Response, next: NextFunction): void {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            next(new ValidationError(errors.mapped()));
            return;
        }

        const command: MoneyWithdrawIssuedCommand = new MoneyWithdrawIssuedCommand(
            res.locals.userId,
            req.body.currency,
            req.body.amount
        );

        this.commandBus.handle(command)
            .then((result: CommandResult) => {
                res
                    .status(200)
                    .send();
            })
            .catch(next);
    }

    cancelOrder(req: Request, res: Response, next: NextFunction): void {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            next(new ValidationError(errors.mapped()));
            return;
        }

        const command: CancelOrderCommand = new CancelOrderCommand(
            res.locals.userId,
            req.params.id
        );

        this.commandBus.handle(command)
            .then((result: CommandResult) => {
                    res
                        .status(200)
                        .send();
            })
            .catch(next);
    }

    issueOrder(req: Request, res: Response, next: NextFunction): void {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            next(new ValidationError(errors.mapped()));
            return;
        }

        const command: CreateOrderCommand = new CreateOrderCommand(
            res.locals.userId,
            req.body.pair,
            req.body.amount,
            req.body.unitPrice,
            req.body.type
        );

        this.commandBus.handle(command)
            .then((result: CommandResult) => {
                res
                    .status(200)
                    .send();
            })
            .catch(next);
    }
}