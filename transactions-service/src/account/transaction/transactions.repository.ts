import { Db } from "mongodb";
import { Transaction } from "./transaction";

export default interface TransactionsRepository
{
    clear(): Promise<any>;
    add(transaction: Transaction): Promise<boolean>;
    findUserTransactions(userId: string): Promise<Transaction[]>;
    findLatestTransactions(limit: number): Promise<Transaction[]>;
}

export class MongoDBTransactionsRepository implements TransactionsRepository {
    private collection: string = 'transactions';

    constructor(
        private connection: Db
    ) {}

    clear(): Promise<any> {
        return this.connection.collection(this.collection).remove({});
    }

    add(transaction: Transaction): Promise<boolean> {
       return this.connection.collection(this.collection)
           .insert(transaction)
           .then(() => Promise.resolve(true));
    }

    findUserTransactions(userId: string): Promise<Transaction[]> {
        return this.connection.collection(this.collection)
            .find({
                userId
            })
            .sort({
                date: -1
            })
            .toArray();
    }

    findLatestTransactions(limit: number): Promise<Transaction[]> {
        return this.connection.collection(this.collection)
            .find()
            .limit(limit)
            .sort({
                date: -1
            })
            .toArray();
    }
}