import { Pair } from "../order/order";
import TransactionEvent from "../events/transaction.event";
import moment = require("moment");

export interface Transaction {
    id: string;
    orderId: string;
    type: string;
    unitPrice: number;
    pair: Pair,
    accountId: string;
    amount: number;
    total: number;
    date: number;
}

export const createFromEventData = (event: TransactionEvent): Transaction => ({
    id: event.getData().id,
    orderId: event.getData().orderId,
    type: event.getData().type,
    unitPrice: event.getData().unitPrice,
    pair: event.getData().pair,
    accountId: event.getData().accountId,
    amount: event.getData().amount,
    total: (event.getData().amount - event.getData().amountLeft) * event.getData().unitPrice,
    date: moment().unix()
});