import { Command } from "../../command-bus/command-bus";

export default class MatchTransactionsCommand implements Command{
    static type: string = 'MatchTransactionsCommand';

    constructor(
        public orderId: string
    ){}

    getType(): string {
        return MatchTransactionsCommand.type;
    }
}