import { Handler } from "../../command-bus/command-bus";
import EventsStore from "../../events/events.repository";
import { Event } from "../../event-sourcing/event.interface";
import Account from "../account";
import MatchTransactionsCommand from "./match-transactions.command";
import ActiveOrdersRepository, { ActiveOrder } from "../order/active-orders.repository";
import TransactionEvent from "../events/transaction.event";

export default class MatchTransactionsHandler implements Handler {
    constructor(private eventsStore: EventsStore, private activeOrdersRepository: ActiveOrdersRepository) {}

    async handle(command: MatchTransactionsCommand): Promise<any> {
        const activeOrder: ActiveOrder | null = await this.activeOrdersRepository.findOrderById(command.orderId);

        if (!activeOrder) {
            throw new Error(`Order with id "${command.orderId}" does not exists`);
        }

        const matchingOrders: ActiveOrder[] = await this.activeOrdersRepository.findMatchingOrders(activeOrder);

        if (matchingOrders.length > 0) {
            const mainAccountEvents: Event[] = await this.eventsStore.findByAggregateId(activeOrder.userId);
            const mainAccount: Account = Account.fromEvents(activeOrder.userId, mainAccountEvents);

            for (const matchingOrder of matchingOrders) {
                const orderAccountEvents: Event[] = await this.eventsStore.findByAggregateId(matchingOrder.userId);
                const orderAccount: Account = Account.fromEvents(matchingOrder.userId, orderAccountEvents);

                mainAccount.addTransaction(activeOrder._id, matchingOrder.left);

                const mainAccountTransactionEvents: Event[] = mainAccount.getRecordedEvents();
                const mainAccountTransactionDetails: TransactionEvent = <TransactionEvent> mainAccountTransactionEvents.find(
                    (event: Event) => event.getType() === TransactionEvent.type
                );

                orderAccount.addTransaction(matchingOrder._id, mainAccountTransactionDetails.getData().amount);

                const orderAccountTransactionEvents: Event[] = orderAccount.getRecordedEvents();

                const events: Event[] = [
                    ...mainAccountTransactionEvents,
                    ...orderAccountTransactionEvents
                ];

                for (const event of events) {
                   await this.eventsStore.save(event);
                }

                if (mainAccountTransactionDetails.getData().amountLeft <= 0) {
                    return Promise.resolve();
                }
            }
        }

        return Promise.resolve();
    }

    getType(): string {
        return MatchTransactionsCommand.type;
    }
}