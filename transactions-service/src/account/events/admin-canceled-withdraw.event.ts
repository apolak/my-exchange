import { Event } from "../../event-sourcing/event.interface";

interface AdminCanceledWithdrawEventData {
    lockId: string;
    issuer: string;
    currency: string;
    amount: number;
}

export default class AdminCanceledWithdrawEvent implements Event {
    static type: string = 'ADMIN_CANCELED_WITHDRAW';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: AdminCanceledWithdrawEventData
    ) {}

    getType(): string {
        return AdminCanceledWithdrawEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): AdminCanceledWithdrawEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}