import { Event } from "../../event-sourcing/event.interface";

export default class AccountCreatedEvent implements Event {
    static type: string = "TRANSACTION_ACCOUNT_CREATED";
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number
    ) {}

    getType(): string {
        return AccountCreatedEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): any {
        return {};
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}