import { Event } from "../../event-sourcing/event.interface";

interface OrderCreatedEventData {
    id: string,
    pair: {
        first: string,
        second: string
    },
    lockId: string,
    amount: number,
    unitPrice: number
}

export default class SellOrderCreatedEvent implements Event {
    static type: string = 'SELL_ORDER_CREATED';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: OrderCreatedEventData
    ) {}

    getType(): string {
        return SellOrderCreatedEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): OrderCreatedEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}