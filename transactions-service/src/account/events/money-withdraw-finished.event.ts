import { Event } from "../../event-sourcing/event.interface";

interface MoneyWithdrawFinishedEventData {
    withdrawId: string;
    issuer: string;
    currency: string;
    amount: number;
}

export default class MoneyWithdrawFinishedEvent implements Event {
    static type: string = 'WITHDRAW_FINISHED';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: MoneyWithdrawFinishedEventData
    ) {}

    getType(): string {
        return MoneyWithdrawFinishedEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): MoneyWithdrawFinishedEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}