import { Event } from "../../event-sourcing/event.interface";

interface AmountLockRemovedEventData {
    lockId: string;
}

export default class AmountLockRemovedEvent implements Event {
    static type: string = 'AMOUNT_LOCK_REMOVED';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: AmountLockRemovedEventData
    ) {}

    getType(): string {
        return AmountLockRemovedEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): AmountLockRemovedEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}