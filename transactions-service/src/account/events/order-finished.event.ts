import { Event } from "../../event-sourcing/event.interface";

interface OrderFinishedEventData {
    orderId: string;
}

export default class OrderFinishedEvent implements Event {
    static type: string = 'ORDER_FINISHED';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: OrderFinishedEventData
    ) {}

    getType(): string {
        return OrderFinishedEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): OrderFinishedEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}