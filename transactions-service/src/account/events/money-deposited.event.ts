import { Event } from "../../event-sourcing/event.interface";

interface MoneyDepositedEventData {
    currency: string;
    amount: number;
}

export default class MoneyDepositedEvent implements Event {
    static type: string = 'MONEY_DEPOSITED';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: MoneyDepositedEventData
    ) {}

    getType(): string {
        return MoneyDepositedEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): MoneyDepositedEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}