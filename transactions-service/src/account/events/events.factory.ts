import { Event } from "../../event-sourcing/event.interface";
import AccountCreatedEvent from "./account-created.event";
import WalletAddedEvent from "./wallet-added.event";
import MoneyDepositedEvent from "./money-deposited.event";
import MoneyWithdrawIssuedEvent from "./money-withdraw-issued.event";
import MoneyWithdrawCanceledEvent from "./money-withdraw-canceled.event";
import AdminCanceledWithdrawEvent from "./admin-canceled-withdraw.event";
import MoneyWithdrawFinishedEvent from "./money-withdraw-finished.event";
import SellOrderCreatedEvent from "./sell-order-created.event";
import BuyOrderCreatedEvent from "./buy-order-created.event";
import OrderCanceledEvent from "./order-canceled.event";
import OrderFinishedEvent from "./order-finished.event";
import AmountLockRemovedEvent from "./amount-lock-removed.event";
import TransactionEvent from "./transaction.event";

export default class EventsFactory {
    static create(eventData: any): Event {
        switch (eventData.type) {
            case OrderFinishedEvent.type:
                return new OrderFinishedEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case AmountLockRemovedEvent.type:
                return new AmountLockRemovedEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case AccountCreatedEvent.type:
                return new AccountCreatedEvent(eventData.version, eventData.aggregateId, eventData.date);
            case WalletAddedEvent.type:
                return new WalletAddedEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case MoneyWithdrawIssuedEvent.type:
                return new MoneyWithdrawIssuedEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case MoneyWithdrawCanceledEvent.type:
                return new MoneyWithdrawCanceledEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case AdminCanceledWithdrawEvent.type:
                return new AdminCanceledWithdrawEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case MoneyDepositedEvent.type:
                return new MoneyDepositedEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case MoneyWithdrawFinishedEvent.type:
                return new MoneyWithdrawFinishedEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case SellOrderCreatedEvent.type:
                return new SellOrderCreatedEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case BuyOrderCreatedEvent.type:
                return new BuyOrderCreatedEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case OrderCanceledEvent.type:
                return new OrderCanceledEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
            case TransactionEvent.type:
                return new TransactionEvent(eventData.version, eventData.aggregateId, eventData.date, eventData.data);
        }

        throw new Error(`Unknown event type: ${eventData.type}`);
    }
}