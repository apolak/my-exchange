import { Event } from "../../event-sourcing/event.interface";

interface WalletAddedEventData {
    currency: string;
}

export default class WalletAddedEvent implements Event {
    static type: string = "WALLET_ADDED";
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: WalletAddedEventData
    ) {}

    getType(): string {
        return WalletAddedEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): WalletAddedEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}