import { Event } from "../../event-sourcing/event.interface";

interface MoneyWithdrawIssuedEventData {
    withdrawId: string;
    currency: string;
    amount: number;
    type: string;
}

export default class MoneyWithdrawIssuedEvent implements Event {
    static type: string = 'WITHDRAW_ISSUED';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: MoneyWithdrawIssuedEventData
    ) {}

    getType(): string {
        return MoneyWithdrawIssuedEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): MoneyWithdrawIssuedEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}