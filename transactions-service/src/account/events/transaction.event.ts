import { Event } from "../../event-sourcing/event.interface";
import { Pair } from "../order/order";

interface TransactionEventData {
    id: string,
    orderId: string,
    unitPrice: number,
    amount: number,
    orderTotalBefore: number,
    orderTotalAfter: number,
    amountLeft: number,
    type: string,
    pair: Pair,
    accountId: string
}

export default class TransactionEvent implements Event {
    static type: string = 'TRANSACTION';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: TransactionEventData
    ) {}

    getType(): string {
        return TransactionEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): TransactionEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}