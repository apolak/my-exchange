import { Event } from "../../event-sourcing/event.interface";
import { Pair } from "../order/order";

interface OrderCanceledEventData {
    orderId: string;
    type: string;
    amount: number;
    unitPrice: number;
    pair: Pair;
}

export default class OrderCanceledEvent implements Event {
    static type: string = 'ORDER_CANCELED';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: OrderCanceledEventData
    ) {}

    getType(): string {
        return OrderCanceledEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): OrderCanceledEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}