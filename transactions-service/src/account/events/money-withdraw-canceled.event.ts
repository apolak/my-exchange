import { Event } from "../../event-sourcing/event.interface";

interface MoneyWithdrawCanceledEventData {
    lockId: string;
    issuer: string;
    currency: string;
    amount: number;
}

export default class MoneyWithdrawCanceledEvent implements Event {
    static type: string = 'WITHDRAW_CANCELED';
    static version: number = 1;

    constructor(
        private version: number ,
        private aggregateId: string,
        private date: number,
        private data: MoneyWithdrawCanceledEventData
    ) {}

    getType(): string {
        return MoneyWithdrawCanceledEvent.type
    }

    getVersion(): number {
        return this.version;
    }

    getAggregateId(): string {
        return this.aggregateId;
    }

    getDate(): number {
        return this.date;
    }

    getData(): MoneyWithdrawCanceledEventData {
        return this.data;
    }

    toJSON(): any {
        return {
            version: this.version,
            aggregateId: this.aggregateId,
            data: this.getData()
        }
    }
}