import { Handler } from "../../command-bus/command-bus";
import EventsStore from "../../events/events.repository";
import { Event } from "../../event-sourcing/event.interface";
import Account from "../account";
import CreateAccountCommand from "./create-account.command";

export default class CreateAccountHandler implements Handler {
    constructor(private eventsStore: EventsStore) {}

    handle(command: CreateAccountCommand): Promise<any> {
        return this.eventsStore.findByAggregateId(command.userId)
            .then((events: Event[]) => {
                if (events.length > 0) {
                    throw new Error(`User with id "${command.userId}" already exists`);
                }

                const account: Account = Account.create(command.userId);
                account.addWallet('BTC');
                account.addWallet('USD');

                account.getRecordedEvents().forEach((event: Event) => this.eventsStore.save(event));
            });
    }

    getType(): string {
        return CreateAccountCommand.type;
    }
}