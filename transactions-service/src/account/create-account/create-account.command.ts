import { Command } from "../../command-bus/command-bus";

export default class CreateAccountCommand implements Command{
    static type: string = 'CreateAccountCommand';

    constructor(public userId: string){}

    getType(): string {
        return CreateAccountCommand.type;
    }
}