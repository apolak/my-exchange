import AmountLock from "./amount-lock";

export default class Wallet {
    private balance: number;
    private locks: AmountLock[];

    constructor(
        private currency: string
    ) {
        this.balance = 0;
        this.locks = [];
    }

    deposit(amount: number) {
        this.balance += amount;
    }

    lock(id: string, amount: number, type: string) {
        this.locks.push(new AmountLock(
            id,
            amount,
            type
        ));

        this.balance -= amount;
    }

    getCurrency(): string {
        return this.currency;
    }

    getLock(id: string): AmountLock {
        const lock: AmountLock|undefined = this.locks.find((lock: AmountLock) => lock.getId() === id);

        if(!lock) {
            throw new Error("Unknown lock");
        }

        return lock;
    }

    updateLock(id: string, amount: number) {
        const lock: AmountLock|undefined = this.locks.find((lock: AmountLock) => lock.getId() === id);

        if (lock) {
            const updatedLock: AmountLock = lock.sub(amount);

            this.locks.filter((lock: AmountLock) => lock.getId() !== id);
            this.locks.push(updatedLock);
        }
    }

    unlock(id: string) {
        const lockIndex: number = this.locks.findIndex((lock: AmountLock) => lock.getId() === id);

        if (lockIndex < 0) {
            throw new Error(`Could not find lock with id "${id}"`);
        }

        this.balance += this.locks[lockIndex].getAmount();
        this.locks = this.locks.filter((lock: AmountLock) => lock.getId() !== id);
    }

    withdraw(id: string) {
        this.locks = this.locks.filter((lock: AmountLock) => lock.getId() !== id);
    }

    getTotalLocked(): number {
        return this.locks.reduce((current: number, lock: AmountLock) => current + lock.getAmount(), 0);
    }

    getTotal(): number {
        return this.balance;
    }

    isForCurrency(currency: string): boolean {
        return this.currency.toLowerCase() === currency.toLowerCase();
    }

    hasEnoughBalance(amount: number): boolean {
        return this.balance >= amount;
    }

    hasLock(lockId: string): boolean {
        return this.locks.findIndex((lock: AmountLock) => lock.getId() === lockId) >= 0;
    }

    guardAmount(amount: number) {
        if (this.balance < amount) {
            throw new Error(`Not enough balance on account "${this.currency}"`);
        }
    }
}