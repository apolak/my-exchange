export default class AmountLock {
    static WITHDRAW = 'withdraw';
    static ORDER = 'order';

    constructor(
        private id: string,
        private amount: number,
        private type: string
    ) {}

    getAmount(): number {
        return this.amount;
    }

    getId(): string {
        return this.id;
    }

    getType(): string {
        return this.type;
    }

    sub(amount: number): AmountLock {
        return new AmountLock(
            this.id,
            this.amount - amount,
            this.type
        );
    }
}