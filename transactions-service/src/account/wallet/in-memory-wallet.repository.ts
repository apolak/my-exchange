interface Wallet {
    available: number,
    locked: number
}

interface UserWallets {
    [currency: string]: Wallet
}

export interface WalletRepository {
    clear(): Promise<boolean>;
    add(userId: string, currency: string): Promise<any>;
    findByUserId(userId: string): Promise<UserWallets|null>;
    updateWalletAvailableAmount(userId: string, currency: string, amount: number): Promise<any>;
    updateWalletLockedAmount(userId: string, currency: string, amount: number): Promise<any>;
}

export default class InMemoryWalletRepository implements WalletRepository {
    private usersWallets: {
        [userId: string]: UserWallets
    } = {};

    clear(): Promise<boolean> {
        this.usersWallets = {};
        return Promise.resolve(true);
    }

    add(userId: string, currency: string): Promise<any> {
        if (typeof  this.usersWallets[userId] === 'undefined') {
            this.usersWallets[userId] = {};
        }

        this.usersWallets[userId][currency.toLowerCase()] = {
            available: 0,
            locked: 0
        };

        return Promise.resolve();
    }

    updateWalletAvailableAmount(userId: string, currency: string, amount: number): Promise<any> {
        this.usersWallets[userId][currency.toLowerCase()].available = this.usersWallets[userId][currency.toLowerCase()].available + amount;
        return Promise.resolve();
    }

    updateWalletLockedAmount(userId: string, currency: string, amount: number): Promise<any> {
        this.usersWallets[userId][currency.toLowerCase()].locked = this.usersWallets[userId][currency.toLowerCase()].locked + amount;
        return Promise.resolve();
    }

    findByUserId(userId: string): Promise<UserWallets|null> {
        if (typeof this.usersWallets[userId] !== 'undefined') {
            return Promise.resolve(this.usersWallets[userId]);
        }

        return Promise.resolve(null);
    }
}