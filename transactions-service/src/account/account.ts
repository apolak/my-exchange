import moment = require("moment");
import Wallet from "./wallet/wallet";
import AccountCreatedEvent from "./events/account-created.event";
import AggregateRoot from "../event-sourcing/aggregate";
import { Event } from "../event-sourcing/event.interface";
import WalletAddedEvent from "./events/wallet-added.event";
import MoneyDepositedEvent from "./events/money-deposited.event";
import MoneyWithdrawIssuedEvent from "./events/money-withdraw-issued.event";
import uuid = require('uuid/v4');
import AmountLock from "./wallet/amount-lock";
import MoneyWithdrawCanceledEvent from "./events/money-withdraw-canceled.event";
import MoneyWithdrawFinishedEvent from "./events/money-withdraw-finished.event";
import AdminCanceledWithdrawEvent from "./events/admin-canceled-withdraw.event";
import Order, { Pair, TransactionDetails } from "./order/order";
import BuyOrderCreatedEvent from "./events/buy-order-created.event";
import SellOrderCreatedEvent from "./events/sell-order-created.event";
import OrderCanceledEvent from "./events/order-canceled.event";
import TransactionEvent from "./events/transaction.event";
import OrderFinishedEvent from "./events/order-finished.event";
import AmountLockRemovedEvent from "./events/amount-lock-removed.event";

export default class Account implements AggregateRoot {
    private recordedEvents: Event[] = [];
    private wallets: Wallet[] = [];
    private orders: Order[] = [];

    constructor(
        private id: string
    ) {
        this.recordedEvents = [];
    }

    static create(userId: string): Account {
        const account: Account = new Account(userId);

        const event: AccountCreatedEvent = new AccountCreatedEvent(
            AccountCreatedEvent.version,
            userId,
            moment().unix()
        );
        account.recordedEvents.push(event);
        account.handleEvent(event);

        return account;
    }

    cancelOrder(orderId: string) {
        this.guardOrderExistence(orderId);

        const order: Order = this.orders.find((order: Order) => order.getId() === orderId);

        const event: OrderCanceledEvent = new OrderCanceledEvent(
            OrderCanceledEvent.version,
            this.id,
            moment().unix(),
            {
                orderId,
                amount: order.getTotalLeft(),
                pair: order.getPair(),
                unitPrice: order.getUnitPrice(),
                type: order.getType()
            }
        );

        this.recordedEvents.push(event);
        this.handleEvent(event);
    }

    addTransaction(orderId: string, amount: number) {
        this.guardOrderExistence(orderId);

        const transactionOrder: Order = this.orders.find((order: Order) => order.getId() === orderId);

        this.guardWalletExistence(transactionOrder.getPair().first, true);
        this.guardWalletExistence(transactionOrder.getPair().second, true);

        const transactionDetails: TransactionDetails = transactionOrder.addTransaction(amount);

        if (transactionOrder.isBuyOrder()) {
            this.getWallet(transactionOrder.getPair().first).deposit(
                amount - transactionDetails.amountLeft
            );
            this.getWallet(transactionOrder.getPair().second).updateLock(
                transactionOrder.getOrderDetails().lockId,
                (amount * transactionOrder.getUnitPrice()) - (transactionDetails.amountLeft * transactionOrder.getUnitPrice())
            );
        } else {
            this.getWallet(transactionOrder.getPair().first).updateLock(
                transactionOrder.getOrderDetails().lockId,
                amount - transactionDetails.amountLeft
            );
            this.getWallet(transactionOrder.getPair().second).deposit(
                (amount * transactionOrder.getUnitPrice()) - (transactionDetails.amountLeft * transactionOrder.getUnitPrice())
            );
        }

        const events: Event[] = [];

        events.push(new TransactionEvent(
            TransactionEvent.version,
            this.id,
            moment().unix(),
            {
                id: uuid(),
                orderId,
                unitPrice: transactionOrder.getUnitPrice(),
                amount,
                orderTotalBefore: transactionDetails.totalLeftBefore,
                orderTotalAfter: transactionDetails.totalLeft,
                amountLeft: transactionDetails.amountLeft,
                type: transactionOrder.isBuyOrder() ? Order.BUY : Order.SELL,
                pair: {
                    first: transactionOrder.getPair().first,
                    second: transactionOrder.getPair().second
                },
                accountId: this.id
            }
        ));

        if (transactionOrder.isFulfilled()) {
            events.push(new AmountLockRemovedEvent(
                OrderFinishedEvent.version,
                this.id,
                moment().unix(),
                {
                    lockId: transactionOrder.getOrderDetails().lockId
                }
            ));

            events.push(new OrderFinishedEvent(
                OrderFinishedEvent.version,
                this.id,
                moment().unix(),
                {
                    orderId
                }
            ));
        }

        events.forEach((event: Event) => {
            this.recordedEvents.push(event);
            this.handleEvent(event);
        });
    }

    makeOrder(type: string, pair: Pair, unitPrice: number, amount: number) {
        this.guardWalletExistence(pair.first, true);
        this.guardWalletExistence(pair.second, true);

        const wallet: Wallet = (type === Order.BUY) ? this.getWallet(pair.second) : this.getWallet(pair.first);
        wallet.guardAmount(
            (type === Order.BUY) ? amount * unitPrice : amount
        );

        let event: Event = null;
        const eventData: any = {
            id: uuid(),
            pair,
            lockId: uuid(),
            amount,
            unitPrice
        };

        if (type === Order.BUY) {
            event = new BuyOrderCreatedEvent(
                BuyOrderCreatedEvent.version,
                this.id,
                moment().unix(),
                eventData
            );
        }

        if (type === Order.SELL) {
            event = new SellOrderCreatedEvent(
                BuyOrderCreatedEvent.version,
                this.id,
                moment().unix(),
                eventData
            );
        }

        this.recordedEvents.push(event);
        this.handleEvent(event);
    }

    deposit(currency: string, amount: number) {
        this.guardWalletExistence(currency, true);

        const event: MoneyDepositedEvent = new MoneyDepositedEvent(
            MoneyDepositedEvent.version,
            this.id,
            moment().unix(),
            {
                currency,
                amount
            }
        );

        this.recordedEvents.push(event);
        this.handleEvent(event);
    }

    withdraw(lockId: string, issuer: string) {
        const wallet: Wallet = this.getWalletByLockId(lockId);

        const event: MoneyWithdrawFinishedEvent = new MoneyWithdrawFinishedEvent(
            MoneyDepositedEvent.version,
            this.id,
            moment().unix(),
            {
                withdrawId: lockId,
                issuer,
                currency: wallet.getCurrency(),
                amount: wallet.getLock(lockId).getAmount()
            }
        );

        this.recordedEvents.push(event);
        this.handleEvent(event);
    }

    cancelWithdraw(withdrawId: string, issuer: string) {
        const wallet: Wallet = this.getWalletByLockId(withdrawId);

        if (this.id === issuer) {
            const event: MoneyWithdrawCanceledEvent = new MoneyWithdrawCanceledEvent(
                MoneyDepositedEvent.version,
                this.id,
                moment().unix(),
                {
                    lockId: withdrawId,
                    issuer,
                    currency: wallet.getCurrency(),
                    amount: wallet.getLock(withdrawId).getAmount()
                }
            );

            this.recordedEvents.push(event);
            this.handleEvent(event);
        } else {
            const event: AdminCanceledWithdrawEvent = new AdminCanceledWithdrawEvent(
                AdminCanceledWithdrawEvent.version,
                this.id,
                moment().unix(),
                {
                    lockId: withdrawId,
                    issuer,
                    currency: wallet.getCurrency(),
                    amount: wallet.getLock(withdrawId).getAmount()
                }
            );

            this.recordedEvents.push(event);
            this.handleEvent(event);
        }
    }

    issueWithdraw(currency: string, amount: number) {
        const wallet: Wallet = this.getWallet(currency);

        if (!wallet.hasEnoughBalance(amount)) {
            throw new Error("Wallet does not have enough amount to withdraw");
        }

        const event: MoneyWithdrawIssuedEvent = new MoneyWithdrawIssuedEvent(
            MoneyDepositedEvent.version,
            this.id,
            moment().unix(),
            {
                withdrawId: uuid(),
                currency,
                amount,
                type: AmountLock.WITHDRAW
            }
        );

        this.recordedEvents.push(event);
        this.handleEvent(event);
    }

    addWallet(currency: string): void {
        this.guardWalletExistence(currency, false);

        const event: WalletAddedEvent = new WalletAddedEvent(
            WalletAddedEvent.version,
            this.id,
            moment().unix(),
            {
                currency: currency.toLowerCase()
            }
        );

        this.recordedEvents.push(event);
        this.handleEvent(event);
    }

    getRecordedEvents(): Event[] {
        const events: Event[] = [...this.recordedEvents];
        this.recordedEvents = [];

        return events;
    }

    static fromEvents(id: string, events: Event[]): Account {
        let account: Account = new Account(id);

        events.forEach((event: Event) => {
            account.handleEvent(event);
        });

        return account;
    }

    private guardOrderExistence(orderId: string) {
        const existingOrder: Order|undefined = this.orders.find((order: Order) => order.getId() === orderId);

        if (!existingOrder) {
            throw new Error(`Order with id "${orderId}" does not exists`);
        }
    }

    private guardWalletExistence(currency: string, checkExistence: boolean): void {
        const existingWallet: Wallet|undefined = this.wallets.find((wallet: Wallet) => wallet.isForCurrency(currency));

        if (checkExistence && !existingWallet) {
            throw new Error(`Wallet "${currency}" does not exist for user "${this.id}"`);
        }

        if (!checkExistence && existingWallet) {
            throw new Error(`Wallet "${currency}" does exist for user "${this.id}"`);
        }
    }

    private getWallet(currency: string): Wallet {
        const existingWallet: Wallet|undefined = this.wallets.find((wallet: Wallet) => wallet.isForCurrency(currency));

        if (!existingWallet) {
            throw new Error(`Wallet "${currency}" does not exist for user "${this.id}"`);
        }

        return existingWallet;
    }

    private getWalletByLockId(lockId: string): Wallet {
        const existingWallet: Wallet|undefined = this.wallets.find((wallet: Wallet) => wallet.hasLock(lockId));

        if (!existingWallet) {
            throw new Error(`There is no wallet for lock "${lockId}"`);
        }

        return existingWallet;
    }

    private handleEvent(event: Event): void {
        switch (event.getType()) {
            case AccountCreatedEvent.type:
                break;
            case TransactionEvent.type:
                const transactionOrder: Order = this.orders.find((order: Order) => order.getId() === event.getData().orderId);
                const transactionDetails: TransactionDetails = transactionOrder.addTransaction(event.getData().amount);

                if (transactionOrder.isBuyOrder()) {
                    this.getWallet(transactionOrder.getPair().first).deposit(
                        event.getData().amount - transactionDetails.amountLeft
                    );
                    this.getWallet(transactionOrder.getPair().second).updateLock(
                        transactionOrder.getOrderDetails().lockId,
                        (event.getData().amount * transactionOrder.getUnitPrice()) - (transactionDetails.amountLeft * transactionOrder.getUnitPrice())
                    );
                } else {
                    this.getWallet(transactionOrder.getPair().first).updateLock(
                        transactionOrder.getOrderDetails().lockId,
                        event.getData().amount - transactionDetails.amountLeft
                    );
                    this.getWallet(transactionOrder.getPair().second).deposit(
                        (event.getData().amount * transactionOrder.getUnitPrice()) - (transactionDetails.amountLeft * transactionOrder.getUnitPrice())
                    );
                }

                break;
            case OrderCanceledEvent.type:
                const order: Order = this.orders.find((order: Order) => order.getId() === event.getData().orderId);

                if (order.getType() === Order.BUY) {
                    this.getWallet(order.getPair().second).unlock(order.getOrderDetails().lockId);
                } else {
                    this.getWallet(order.getPair().first).unlock(order.getOrderDetails().lockId);
                }

                this.orders = this.orders.filter((order: Order) => order.getId() !== event.getData().orderId);
                break;
            case BuyOrderCreatedEvent.type:
                this.orders.push(
                    Order.buy(
                        event.getData().id,
                        event.getData().pair,
                        event.getData().unitPrice,
                        {
                            amount: event.getData().amount,
                            lockId: event.getData().lockId
                        }
                    )
                );
                this.getWallet(event.getData().pair.second).lock(event.getData().lockId, event.getData().amount * event.getData().unitPrice, AmountLock.ORDER);
                break;
            case SellOrderCreatedEvent.type:
                this.orders.push(
                    Order.sell(
                        event.getData().id,
                        event.getData().pair,
                        event.getData().unitPrice,
                        {
                            amount: event.getData().amount,
                            lockId: event.getData().lockId
                        }
                    )
                );

                this.getWallet(event.getData().pair.first).lock(event.getData().lockId, event.getData().amount, AmountLock.ORDER);
                break;
            case MoneyWithdrawIssuedEvent.type:
                this.getWallet(event.getData().currency).lock(
                    event.getData().withdrawId,
                    event.getData().amount,
                    event.getData().type
                );
                break;
            case MoneyWithdrawCanceledEvent.type:
            case AdminCanceledWithdrawEvent.type:
                this.getWalletByLockId(event.getData().lockId).unlock(
                    event.getData().lockId
                );
                break;

            case MoneyWithdrawFinishedEvent.type:
                this.getWalletByLockId(event.getData().withdrawId).withdraw(
                    event.getData().withdrawId
                );
                break;
            case MoneyDepositedEvent.type:
                this.getWallet(event.getData().currency).deposit(event.getData().amount);
                break;
            case WalletAddedEvent.type:
                this.wallets.push(new Wallet(event.getData().currency));
                break;

        }
    }
}