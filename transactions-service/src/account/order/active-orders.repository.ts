import { Db } from "mongodb";
import Order, { Pair } from "./order";

export interface ActiveOrder {
    _id: string;
    type: string;
    unitPrice: number;
    pair: Pair,
    left: number;
    userId: string;
    amount: number;
}

export default interface ActiveOrdersRepository
{
    findOrderById(orderId: string): Promise<ActiveOrder|null>;
    findMatchingOrders(order: ActiveOrder): Promise<ActiveOrder[]>;
    findOrders(limit: number): Promise<{ buy: ActiveOrder[]; sell: ActiveOrder[]}>;
    add(order: ActiveOrder): Promise<any>;
    update(orderId: string, amount: number): Promise<any>;
    remove(orderId: string): Promise<any>;
    clear(): Promise<boolean>;
}

export class MongoDBActiveOrdersRepository implements ActiveOrdersRepository {
    private collection: string = 'write:active-orders';

    constructor(
        private connection: Db
    ) {}

    clear(): Promise<boolean> {
        return this.connection.collection(this.collection)
            .remove({}).then(() => Promise.resolve(true));
    }

    async findOrders(limit: number): Promise<{ buy: ActiveOrder[]; sell: ActiveOrder[]}> {
        const buyOrders: ActiveOrder[] = await this.connection.collection(this.collection)
            .find({
                type: Order.BUY
            })
            .limit(limit)
            .sort({
                unitPrice: -1
            })
            .toArray();

        const sellOrders: ActiveOrder[] = await this.connection.collection(this.collection)
            .find({
                type: Order.SELL
            })
            .limit(limit)
            .sort({
                unitPrice: 1
            })
            .toArray();

        return Promise.resolve({
            buy: buyOrders,
            sell: sellOrders
        });
    }

    async findOrderById(orderId: string): Promise<ActiveOrder|null> {
        return await this.connection.collection(this.collection)
            .findOne({
                _id: orderId
            });
    }

    async findMatchingOrders(order: ActiveOrder): Promise<ActiveOrder[]> {
        let query: any = {};

        if (order.type === Order.BUY) {
            query = {
                unitPrice: {
                    $lte: +order.unitPrice
                },
                type: Order.SELL
            };
        } else {
            query = {
                unitPrice: {
                    $gte: +order.unitPrice
                },
                type: Order.BUY
            };
        }

        return await this.connection.collection(this.collection)
            .find(query)
            .sort({
                unitPrice: order.type === Order.BUY ? 1 : -1
            })
            .toArray();
    }

    async add(order: ActiveOrder): Promise<any> {
        return this.connection.collection(this.collection).insert(order);
    }

    async remove(orderId: string): Promise<any> {
        return this.connection.collection(this.collection).remove({
            _id: orderId
        });
    }

    async update(orderId: string, amount: number): Promise<any> {
        return this.connection.collection(this.collection).update(
            {
                _id: orderId
            },
            {
                left: amount
            });
    }
}