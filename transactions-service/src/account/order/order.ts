export interface Pair {
    first: string;
    second: string;
}

interface OrderDetails {
    amount: number;
    lockId: string;
}

interface Transaction {
    amount: number
}

export interface TransactionDetails {
    totalLeft: number;
    totalLeftBefore: number;
    amountLeft: number
}

export default class Order {
    static BUY: string = 'buy';
    static SELL: string = 'sell';

    private transactions: Transaction[] = [];

    constructor(
        private id: string,
        private type: string,
        private pair: Pair,
        private unitPrice: number,
        private details: OrderDetails
    ) {}

    static buy(
        id: string,
        pair: Pair,
        unitPrice: number,
        details: OrderDetails
    ): Order {
        return new Order(
            id,
            Order.BUY,
            pair,
            unitPrice,
            details
        );
    }

    static sell(
        id: string,
        pair: Pair,
        unitPrice: number,
        details: OrderDetails
    ): Order {
        return new Order(
            id,
            Order.SELL,
            pair,
            unitPrice,
            details
        );
    }

    addTransaction(amount: number): TransactionDetails  {
        const totalLeftBefore: number = this.getTotalLeft();
        let amountLeft: number = 0;

        if (amount <= this.getTotalLeft()) {
            this.transactions.push({
                amount
            });
        } else {
            this.transactions.push({
                amount: this.getTotalLeft()
            });

            amountLeft = amount - this.getTotalLeft();
        }

        return {
            totalLeft: this.getTotalLeft(),
            totalLeftBefore,
            amountLeft
        };
    }

    getTotal(): number {
        return this.details.amount;
    }

    isBuyOrder(): boolean {
        return this.type === Order.BUY;
    }

    getTotalLeft(): number {
        const totalTransactions: number = this.transactions.reduce((current: number, transaction: Transaction) => current + transaction.amount, 0);

        return this.getTotal() - totalTransactions;
    }

    getType(): string {
        return this.type;
    }

    isFulfilled(): boolean {
        return this.getTotalLeft() <= 0;
    }

    getId(): string {
        return this.id;
    }

    getPair(): Pair {
        return this.pair;
    }

    getUnitPrice(): number {
        return this.unitPrice;
    }

    getOrderDetails(): OrderDetails {
        return this.details;
    }
}