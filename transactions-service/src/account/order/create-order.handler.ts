import { Handler } from "../../command-bus/command-bus";
import EventsStore from "../../events/events.repository";
import { Event } from "../../event-sourcing/event.interface";
import Account from "../account";
import CreateOrderCommand from "./create-order.command";

export default class CreateOrderHandler implements Handler {
    constructor(private eventsStore: EventsStore) {}

    handle(command: CreateOrderCommand): Promise<any> {
        return this.eventsStore.findByAggregateId(command.userId)
            .then((events: Event[]) => {
                if (events.length <= 0) {
                    throw new Error(`User with id "${command.userId}" does not exists`);
                }

                const account: Account = Account.fromEvents(command.userId, events);

                account.makeOrder(
                    command.type,
                    command.pair,
                    command.unitPrice,
                    command.amount
                );

                account.getRecordedEvents().forEach((event: Event) => this.eventsStore.save(event));
            });
    }

    getType(): string {
        return CreateOrderCommand.type;
    }
}