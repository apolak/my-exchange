import { Command } from "../../command-bus/command-bus";

export default class CreateOrderCommand implements Command{
    static type: string = 'CreateOrderCommand';

    constructor(
        public userId: string,
        public pair: {
            first: string,
            second: string
        },
        public amount: number,
        public unitPrice: number,
        public type: string
    ){}

    getType(): string {
        return CreateOrderCommand.type;
    }
}