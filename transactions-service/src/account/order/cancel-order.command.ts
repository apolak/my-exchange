import { Command } from "../../command-bus/command-bus";

export default class CancelOrderCommand implements Command{
    static type: string = 'CancelOrderCommand';

    constructor(
        public userId: string,
        public orderId: string
    ){}

    getType(): string {
        return CancelOrderCommand.type;
    }
}