import dotenv = require('dotenv');
import { winstonLogger } from "../shared/logger/winston.logger";
import ServiceBusClient from "../shared/service-bus/service-bus.client";
import {
    WITHDRAW_CANCELED, WITHDRAW_ISSUED
} from "../shared/service-bus/events";
import { WITHDRAW_CANCELED_BY_ADMIN, WITHDRAW_FINISHED_BY_ADMIN } from "./events";

dotenv.config();

const serviceBus: ServiceBusClient = new ServiceBusClient('finance_service', `ws://${process.env.SERVICE_BUS_HOST}:${process.env.SERVICE_BUS_PORT}`);

let withdraws: any[] = [];

serviceBus.connect()
    .then(() => {
        winstonLogger.info("Finance service ready");
        serviceBus.subscribe(WITHDRAW_ISSUED, (message: any) => {
            winstonLogger.info(message);

            withdraws.push({
                userId: message.aggregateId,
                lockId: message.data.withdrawId,
                amount: message.data.amount,
                currency: message.data.currency
            });

            setTimeout(() => {
                withdraws = withdraws.filter((withdraw) => withdraw.lockId !== message.data.withdrawId);

                if (Math.random() >= 0.5) {
                    winstonLogger.info(`Withdraw finished by admin`);
                    serviceBus.publish(WITHDRAW_FINISHED_BY_ADMIN, {
                        userId: message.aggregateId,
                        lockId: message.data.withdrawId,
                        issuer: 'SOME_ISSUER'
                    });
                } else {
                    winstonLogger.info(`Withdraw canceled by admin `);
                    serviceBus.publish(WITHDRAW_CANCELED_BY_ADMIN, {
                        userId: message.aggregateId,
                        lockId: message.data.withdrawId,
                        issuer: 'SOME_ISSUER'
                    });
                }

            }, Math.random() * 90000 + 60000);
        });

        serviceBus.subscribe(WITHDRAW_CANCELED, (message: any) => {
            winstonLogger.info(`Withdraw canceled by user`);

            withdraws = withdraws.filter((withdraw) => withdraw.userId !== message.userId && withdraw.lockId !== message.data.lockId);
        });
    })
    .catch(err => winstonLogger.error(err));
